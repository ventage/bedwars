package eu.ventage.bedwars.listeners;

import de.dytanic.cloudnet.bridge.CloudServer;
import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.maps.MapManager;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

public class ServerListPingListener implements Listener {

    public ServerListPingListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onServerPing( ServerListPingEvent event ) {
        MapManager mapManager = BedWars.getInstance().getMapManager();
        if ( mapManager.getActiveMap() != null ) {
            event.setMaxPlayers( mapManager.getActiveMap().getTeamSizes().getServerSize() );
            event.setMotd( mapManager.getActiveMap().getMapName() );
            CloudServer.getInstance().updateAsync();
        }
    }

}
