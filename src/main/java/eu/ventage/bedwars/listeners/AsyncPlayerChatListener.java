package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.teams.Team;
import eu.ventage.bedwars.teams.TeamManager;
import eu.ventage.bedwars.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {

    public AsyncPlayerChatListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onChat( AsyncPlayerChatEvent event ) {
        BedWarsPlayer player = BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() );
        switch ( BedWars.getInstance().getGameState() ) {
            case LOBBY:
            case END:
                event.setFormat( Rank.getTeamByPlayerPermission( event.getPlayer() ).getColor() + event.getPlayer().getName() + " §8» §7" + event.getMessage() );
                break;
            case INGAME:
                if ( player.getTeam() > 0 ) {
                    Team team = Team.getTeamByNumber( player.getTeam() );
                    if ( event.getMessage().startsWith( "@a" ) ) {
                        event.setFormat( "§8[@All] " + team.getColor() + event.getPlayer().getName() + " §8» §7" + event.getMessage().replace( "@a ", "" ) );
                        return;
                    }
                    event.setCancelled( true );
                    for ( Player player1 : BedWars.getInstance().getTeamManager().getPlayers( team ) ) {
                        player1.sendMessage( team.getColor() + event.getPlayer().getName() + " §8» §7" + event.getMessage() );
                    }
                    return;
                }
                event.setCancelled( true );
                for ( Player player1 : Bukkit.getOnlinePlayers() ) {
                    BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( player1.getUniqueId() );
                    if ( bedWarsPlayer.getTeam() < 0 ) {
                        player1.sendMessage( "§7Spec | " + event.getPlayer().getName() + " §8» §7" + event.getMessage() );
                    }
                }
                break;
        }

    }

}
