package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.inventory.LobbyInventory;
import eu.ventage.bedwars.items.ItemBuilder;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shop.ShopManager;
import eu.ventage.bedwars.shopitems.*;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;

public class InventoryClickListener implements Listener {

    public InventoryClickListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onClick( InventoryClickEvent event ) {
        if ( event.getCurrentItem() == null ) return;
        if ( event.getClickedInventory() == null ) return;
        if ( event.getCurrentItem().getType() == null ) return;
        if ( BedWarsPlayer.getBedWarsPlayer( event.getWhoClicked().getUniqueId() ).isBuildMode() ) return;
        if ( BedWars.getInstance().getGameState().equals( GameState.LOBBY ) || BedWars.getInstance().getGameState().equals( GameState.END ) ) {
            if ( event.getClickedInventory().equals( event.getWhoClicked().getInventory() ) ) {
                event.setCancelled( true );
                return;
            }
            switch ( event.getClickedInventory().getTitle() ) {
                case "§aTeam Auswahl":
                    if ( event.getCurrentItem().getType().equals( Material.WOOL ) ) {
                        event.setCancelled( true );
                        BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( event.getWhoClicked().getUniqueId() );
                        Team team = Team.getTeamByString( resetColor( event.getCurrentItem().getItemMeta().getDisplayName() ) );
                        if ( team.getTeam() != bedWarsPlayer.getTeam() ) {
                            BedWars.getInstance().getTeamManager().addPlayer( team, (Player) event.getWhoClicked() );
                            event.getWhoClicked().sendMessage( Data.prefix + "§7Du bist nun dem Team " + team.getColor() + team.getName() + " §a§obeigetreten" );
                            bedWarsPlayer.getScoreboardManager().update();
                            LobbyInventory.setTeamInventory( (Player) event.getWhoClicked() );
                            return;
                        }
                        bedWarsPlayer.getPlayer().sendMessage( Data.prefix + "§7Du bist schon im Team" );
                    }
                    break;
                case "§eShop Einstellungen":
                    break;
            }
        }

        switch ( event.getClickedInventory().getTitle() ) {
            case "§aShop":
                event.setCancelled( true );
                for ( Category category : Category.values() ) {
                    if ( category.getMaterial().equals( event.getCurrentItem().getType() ) ) {
                        BedWarsPlayer.getBedWarsPlayer( event.getWhoClicked().getUniqueId() ).getShopInventory().enterSubTrade( category );
                        return;
                    }
                }
                break;
        }

        for ( Category category : Category.values() ) {
            if ( event.getClickedInventory().getTitle().equalsIgnoreCase( category.getName() ) ) {
                event.setCancelled( true );
                if ( event.getCurrentItem().getType().equals( Material.BARRIER ) && event.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase( "§4Zurück" ) ) {
                    BedWarsPlayer.getBedWarsPlayer( event.getWhoClicked().getUniqueId() ).getShopInventory().openMainInv();
                    return;
                }
                for ( ShopItem shopItem : BedWars.getInstance().getShopManager().getItemsFromCategory( category ) ) {
                    if ( event.getCurrentItem().getType().equals( shopItem.material() ) || event.getClickedInventory().getItem( event.getSlot() - 9 ).getType().equals( shopItem.material() ) ) {
                        if ( event.getAction().equals( InventoryAction.MOVE_TO_OTHER_INVENTORY ) ) {
                            int times = 64 / shopItem.amount();
                            for ( int i = 0; i < times; i++ ) {
                                if ( event.getWhoClicked().getInventory().contains( shopItem.priceItem().getMaterial(), shopItem.price() ) )
                                    buyItem( (Player) event.getWhoClicked(), shopItem );
                                else break;
                            }
                            return;
                        } else {
                            if ( event.getWhoClicked().getInventory().contains( shopItem.priceItem().getMaterial(), shopItem.price() ) ) {
                                buyItem( (Player) event.getWhoClicked(), shopItem );
                                return;
                            }
                        }
                        event.getWhoClicked().sendMessage( Data.prefix + "§7Du hast §c§onicht §7genug Ressourcen" );
                        return;
                    }
                }
                return;
            }
        }
    }

    private String resetColor( String string ) {
        return string
                .replace( "§a", "" )
                .replace( "§b", "" )
                .replace( "§c", "" )
                .replace( "§d", "" )
                .replace( "§e", "" )
                .replace( "§f", "" )
                .replace( "§1", "" )
                .replace( "§2", "" )
                .replace( "§3", "" )
                .replace( "§4", "" )
                .replace( "§5", "" )
                .replace( "§6", "" )
                .replace( "§7", "" )
                .replace( "§8", "" )
                .replace( "§9", "" )
                .replace( "§0", "" )
                .replace( "§l", "" )
                .replace( "§k", "" )
                .replace( "§n", "" )
                .replace( "§m", "" );
    }

    private boolean buyItem( Player player, ShopItem shopItem ) {
        if ( player.getInventory().contains( shopItem.priceItem().getMaterial(), shopItem.price() ) ) {
            HashMap<Integer, ? extends ItemStack> all = player.getInventory().all( shopItem.priceItem().getMaterial() );
            int toRemove = shopItem.price();
            for ( ItemStack itemStack : all.values() ) {
                if ( itemStack.getItemMeta().getDisplayName() == shopItem.priceItem().getName() ) {
                    if ( toRemove >= itemStack.getAmount() ) {
                        toRemove = toRemove - itemStack.getAmount();
                        player.getInventory().remove( itemStack );
                    } else if ( toRemove <= itemStack.getAmount() ) {
                        int amount = itemStack.getAmount() - toRemove;
                        itemStack.setAmount( amount );
                        toRemove = 0;
                        break;
                    }
                }
            }
            if ( toRemove == 0 ) {
                ItemBuilder itemBuilder = new ItemBuilder( shopItem.material() ).setDisplayName( shopItem.displayName() ).setAmount( shopItem.amount() ).setSubID( shopItem.subID() );
                if ( shopItem instanceof ColoredShopItem )
                    itemBuilder.setLeatherColor( ( (ColoredShopItem) shopItem ).color( BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() ) ) );
                if ( shopItem instanceof EnchantedShopItem )
                    ( (EnchantedShopItem) shopItem ).enchantmentList().forEach( itemBuilder::enchant );
                if ( shopItem instanceof ChangableShopItem )
                    itemBuilder.setMaterial( ( (ChangableShopItem) shopItem ).material( BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() ) ) );
                player.getInventory().addItem( itemBuilder.build() );
                return true;
            }
        }
        return false;
    }

}
