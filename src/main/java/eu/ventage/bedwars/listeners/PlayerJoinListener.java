package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.inventory.LobbyInventory;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.runnable.LobbyCountdown;
import eu.ventage.bedwars.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    public PlayerJoinListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onJoin( PlayerJoinEvent event ) {
        BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).set();
        event.setJoinMessage( null );
        if ( BedWars.getInstance().getGameState().equals( GameState.LOBBY ) ) {
            event.setJoinMessage( Data.prefix + Rank.getTeamByPlayerPermission( event.getPlayer() ).getColor() + event.getPlayer().getName() + " §7ist dem Spiel §a§obeigetreten" );
            LobbyInventory.setInventory( event.getPlayer() );
            if ( Bukkit.getOnlinePlayers().size() == BedWars.getInstance().getMapManager().getActiveMap().getTeamSizes().getPlayersToStart() ) {
                BedWars.getInstance().getLobbyCountdown().start();
            }
            event.getPlayer().setGameMode( GameMode.SURVIVAL );
            if ( BedWars.getInstance().getMapManager().getLobby() == null ) {
                if ( event.getPlayer().hasPermission( BedWars.getInstance().getCommand( "setup" ).getPermission() ) ) {
                    event.getPlayer().sendMessage( Data.prefix + "§4Lobbyspawn nicht gesetzt" );
                }
                return;
            }
            event.getPlayer().teleport( BedWars.getInstance().getMapManager().getLobby() );
            return;
        }
        event.getPlayer().teleport( BedWars.getInstance().getMapManager().getActiveMap().getSpectator() );
        event.getPlayer().setGameMode( GameMode.SPECTATOR );
    }

}
