package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.maps.Map;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.teams.Team;
import eu.ventage.bedwars.teams.TeamManager;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import java.util.ArrayList;
import java.util.List;

public class PlayerDeathListener implements Listener {

    public PlayerDeathListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onDeath( PlayerDeathEvent event ) {
        event.setKeepInventory( true );
        event.getEntity().getInventory().clear();
        event.getEntity().getInventory().setArmorContents( null );
        event.getEntity().spigot().respawn();
        event.setDeathMessage( null );
        BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( event.getEntity().getUniqueId() );
        TeamManager teamManager = BedWars.getInstance().getTeamManager();
        Team team = Team.getTeamByNumber( bedWarsPlayer.getTeam() );
        Map map = BedWars.getInstance().getMapManager().getActiveMap();
        if ( teamManager.hasBed( team ) ) {
            respawn( event.getEntity(), map );
        } else {
            if ( bedWarsPlayer.getLastDamager() != null ) {
                if ( ( bedWarsPlayer.getLastDamagerTime() - System.currentTimeMillis() ) < 10000 ) {
                    BedWarsPlayer bedwarstarget = BedWarsPlayer.getBedWarsPlayer( bedWarsPlayer.getLastDamager().getUniqueId() );
                    bedwarstarget.getStats().addTempKills( 1 );
                    Bukkit.broadcastMessage( Data.prefix + Team.getTeamByNumber( bedWarsPlayer.getTeam() ).getColor() + event.getEntity().getName()
                            + " §7wurde von " + Team.getTeamByNumber( bedwarstarget.getTeam() ).getColor() + bedwarstarget.getPlayer().getName() + " §7getötet" );
                }
            } else
                Bukkit.broadcastMessage( Data.prefix + Team.getTeamByNumber( bedWarsPlayer.getTeam() ).getColor() + event.getEntity().getName() + "§7 ist gestorben" );
            event.getEntity().teleport( map.getSpectator() );
            event.getEntity().setGameMode( GameMode.SPECTATOR );
            event.getEntity().sendMessage( Data.prefix + "§7Du bist nun Spectator" );
            bedWarsPlayer.setTeam( -1 );
            bedWarsPlayer.getActionbar().setText( "§7Spectator" );
            BedWars.getInstance().getTeamManager().removePlayer( event.getEntity() );
            for ( Player player : Bukkit.getOnlinePlayers() ) {
                BedWarsPlayer bedWarsPlayer1 = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
                bedWarsPlayer1.getScoreboardManager().updateSideboard();
                bedWarsPlayer1.getScoreboardManager().update();
                if ( bedWarsPlayer1.getTeam() > 0 ) player.hidePlayer( event.getEntity() );
            }
            if ( teamManager.getPlayers( team ).size() < 1 ) {
                Bukkit.broadcastMessage( Data.prefix + "§7Das Team " + team.getColor() + team.getName() + " §7ist ausgeschieden" );
                List<Team> teamsAlive = new ArrayList<>();
                for ( Team team1 : teamManager.getTeams() )
                    if ( teamManager.getPlayers( team1 ).size() > 0 ) teamsAlive.add( team1 );
                if ( teamsAlive.size() < 2 ) {
                    Bukkit.broadcastMessage( Data.prefix + "§7Das Spiel ist §c§ovorbei§7! Team " + teamsAlive.get( 0 ).getColor() + teamsAlive.get( 0 ).getName() + " §7hat gewonnen" );
                    BedWars.getInstance().setGameState( GameState.END );
                    for ( Team team1 : teamsAlive )
                        for ( Player player : teamManager.getPlayers( team1 ) ) {
                            BedWarsPlayer bedWarsPlayer1 = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
                            bedWarsPlayer1.getStats().addGamesWon( 1 );
                        }
                    for ( Player player : Bukkit.getOnlinePlayers() ) {
                        Bukkit.getOnlinePlayers().forEach( o -> player.showPlayer( o ) );
                        player.getInventory().setArmorContents( null );
                        player.setGameMode( GameMode.SURVIVAL );
                        player.getInventory().clear();
                        player.setHealth( 20 );
                        player.setFoodLevel( 20 );
                        player.teleport( BedWars.getInstance().getMapManager().getLobby() );
                        BedWarsPlayer bedWarsPlayer1 = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
                        bedWarsPlayer1.getScoreboardManager().update();
                        bedWarsPlayer1.getScoreboardManager().updateSideboard();
                        bedWarsPlayer1.getStats().save();
                        BedWars.getInstance().getMapManager().getSpawner().cancel();
                    }
                    Bukkit.broadcastMessage( Data.prefix + "§7Der Server stoppt in §e10 §7Sekunden!" );
                    Bukkit.getScheduler().runTaskLater( BedWars.getInstance(), ( ) -> Bukkit.shutdown(), 20 * 10 );
                }
            }
        }

    }

    private void respawn( Player player, Map map ) {
        player.sendMessage( Data.prefix + "§7Dein Bett wurde noch nicht abgebaut, deshalb kannst du respawnen" );
        player.getInventory().clear();
        player.getInventory().setArmorContents( null );
        Bukkit.getScheduler().runTaskLater( BedWars.getInstance(), ( ) -> player.teleport( map.getTeams().get( BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() ).getTeam() - 1 ) ), 2 * 10 );
    }

}
