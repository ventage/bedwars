package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class PlayerPickUpListener implements Listener {

    public PlayerPickUpListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onPickUp( PlayerPickupItemEvent event ) {
        switch ( BedWars.getInstance().getGameState() ) {
            case INGAME:
                event.setCancelled( false );
                break;
            default:
                event.setCancelled( true );
                break;
        }
    }

}
