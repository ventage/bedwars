package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.maps.Map;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.material.Bed;

import java.util.concurrent.ConcurrentHashMap;

public class BlockBreakListener implements Listener {

    public BlockBreakListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onBreak( BlockBreakEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).isBuildMode() ) return;
        if ( !BedWars.getInstance().getGameState().equals( GameState.INGAME ) ) {
            event.setCancelled( true );
            return;
        }
        switch ( event.getBlock().getType() ) {
            case BED:
            case BED_BLOCK:
                event.setCancelled( false );
                BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() );
                event.getBlock().getDrops().clear();
                Block block = event.getBlock().getRelative( BlockFace.DOWN );
                Team team = Team.getTeamByWoolID( block.getData() );
                if ( team != null ) {
                    if ( team.getTeam() != bedWarsPlayer.getTeam() ) {
                        if ( BedWars.getInstance().getTeamManager().hasBed( team ) ) {
                            event.getBlock().setType( Material.AIR );
                            BedWars.getInstance().getTeamManager().removeBed( team );
                            Bukkit.broadcastMessage( Data.prefix + Team.getTeamByNumber( bedWarsPlayer.getTeam() ).getColor() + event.getPlayer().getName() + "§7 hat das Bett von Team " + team.getColor() + team.getName() + " §7abgebaut" );
                            bedWarsPlayer.getStats().addTempBedsDestroyed( 1 );
                            for ( Player players : Bukkit.getOnlinePlayers() ) {
                                players.playSound( players.getLocation(), Sound.WITHER_DEATH, 2, 1 );
                                BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getScoreboardManager().updateSideboard();
                            }
                        } else {
                            event.setCancelled( true );
                            event.getPlayer().sendMessage( Data.prefix + "§7Kein Stats-Farming §a§oerlaubt§7!" );
                        }
                        return;
                    } else {
                        event.setCancelled( true );
                        event.getPlayer().sendMessage( Data.prefix + "§7Du kannst dein eigenes Bett nicht abbauen" );
                    }
                } else
                    System.out.println( event.getPlayer().getName() + " hat versucht ein nicht bekanntes Team abzubauen" );
            default:
                boolean boo = !Data.blocks.contains( event.getBlock() );
                event.setCancelled( boo );
                if ( !boo ) Data.blocks.remove( event.getBlock() );
                break;
        }
    }

}
