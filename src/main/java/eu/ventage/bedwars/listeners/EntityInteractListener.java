package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class EntityInteractListener implements Listener {

    public EntityInteractListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onEntityInteract( PlayerInteractEntityEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).isBuildMode() ) return;
        if ( event.getRightClicked() instanceof Villager ) {
            event.setCancelled( true );
            event.getRightClicked().eject();
            BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getShopInventory().openMainInv();
        }
    }

}
