package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

public class EntitySpawnListener implements Listener {

    public EntitySpawnListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onSpawn( EntitySpawnEvent event ) {
        if ( !event.getEntityType().equals( EntityType.VILLAGER ) &&
                !event.getEntityType().equals( EntityType.SHEEP ) &&
                !event.getEntityType().equals( EntityType.DROPPED_ITEM ) ) {
            event.setCancelled( true );
        }
    }

}
