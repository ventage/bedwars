package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.scoreboard.SidebarBuilder;
import eu.ventage.bedwars.scoreboard.SidebarManager;
import eu.ventage.bedwars.scoreboard.SmartScoreboard;
import eu.ventage.bedwars.teams.TeamManager;
import eu.ventage.bedwars.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuitListener implements Listener {

    public PlayerQuitListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onQuit( PlayerQuitEvent event ) {
        SmartScoreboard.removeUser( event.getPlayer() );
        if ( TeamManager.hasTeam( event.getPlayer() ) )
            BedWars.getInstance().getTeamManager().removePlayer( event.getPlayer() );
        if ( BedWars.getInstance().getGameState().equals( GameState.LOBBY ) ) {
            event.setQuitMessage( Data.prefix + Rank.getTeamByPlayerPermission( event.getPlayer() ).getColor() + event.getPlayer().getName() + " §7hat das Spiel §c§overlassen" );
            BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getScoreboardManager().remove();
            BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).unregister();
        }
        /*
        for (Player player : Bukkit.getOnlinePlayers()) {
            BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer(player.getUniqueId());
            bedWarsPlayer.getScoreboardManager().updateSideboard();
        }
        */
    }

}
