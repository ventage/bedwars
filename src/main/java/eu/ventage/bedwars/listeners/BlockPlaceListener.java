package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.MetadataValue;

public class BlockPlaceListener implements Listener {

    public BlockPlaceListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onBlockPlace( BlockPlaceEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).isBuildMode() ) return;
        Data.blocks.add( event.getBlock() );
        if ( !BedWars.getInstance().getGameState().equals( GameState.INGAME ) ) {
            event.setCancelled( true );
        }
    }

}
