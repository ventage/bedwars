package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.items.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityExplodeEvent;

public class EntityExplosionListener implements Listener {

    public EntityExplosionListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onExplosion( EntityExplodeEvent event ) {
        if ( !event.getEntityType().equals( EntityType.PRIMED_TNT ) ) return;
        event.setCancelled( true );
        for ( Block block : event.blockList() ) {
            if ( Data.blocks.contains( block ) ) {
                block.getLocation().getWorld().dropItem( block.getLocation(), new ItemBuilder( block.getType() ).build() );
                block.setType( Material.AIR );
            }
        }
    }

}
