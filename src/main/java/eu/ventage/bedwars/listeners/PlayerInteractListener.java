package eu.ventage.bedwars.listeners;

import de.dytanic.cloudnet.api.CloudAPI;
import de.dytanic.cloudnet.bridge.CloudServer;
import de.dytanic.cloudnet.lib.player.CloudPlayer;
import de.dytanic.cloudnet.lib.server.info.ServerInfo;
import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.inventory.LobbyInventory;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener implements Listener {

    public PlayerInteractListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).isBuildMode() ) return;
        if ( BedWars.getInstance().getGameState().equals( GameState.LOBBY ) ) {
            if ( event.getAction().equals( Action.RIGHT_CLICK_BLOCK )
                    || event.getAction().equals( Action.RIGHT_CLICK_AIR ) ) {
                if ( !BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).isBuildMode() ) {
                    event.setCancelled( true );
                    switch ( event.getMaterial() ) {
                        case BED:
                            LobbyInventory.setTeamInventory( event.getPlayer() );
                            break;
                        case NETHER_STAR:
                            LobbyInventory.setStatsInventory( event.getPlayer() );
                            break;
                        case MONSTER_EGG:
                            LobbyInventory.setShopEinstellungen( event.getPlayer() );
                            break;
                        case SKULL_ITEM:
                            CloudPlayer onlinePlayer = CloudAPI.getInstance().getOnlinePlayer( event.getPlayer().getUniqueId() );
                            int size = CloudAPI.getInstance().getServers( "Lobby" ).size();
                            onlinePlayer.getPlayerExecutor().sendPlayer( onlinePlayer, "Lobby-" + size );
                            break;
                    }
                }
            }
        } else if ( BedWars.getInstance().getGameState().equals( GameState.INGAME ) ) {
            if ( event.getClickedBlock() == null ) return;
            if ( !event.getClickedBlock().getType().equals( Material.ENDER_CHEST ) ) return;
            if ( !event.getAction().equals( Action.RIGHT_CLICK_BLOCK ) ) return;
            event.setCancelled( true );
            event.getPlayer().eject();
            Team team = Team.getTeamByNumber( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getTeam() );
            team.getTeamChest().open( event.getPlayer() );
        }
    }

}
