package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class PlayerDropListener implements Listener {

    public PlayerDropListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onDrop( PlayerDropItemEvent event ) {
        switch ( BedWars.getInstance().getGameState() ) {
            case INGAME:
                event.setCancelled( false );
                break;
            default:
                event.setCancelled( true );
                break;
        }
    }

}
