package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class EntityDamageListener implements Listener {

    public EntityDamageListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler
    public void onEntityDamage( EntityDamageEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getEntity().getUniqueId() ).isBuildMode() ) return;
        switch ( BedWars.getInstance().getGameState() ) {
            case LOBBY:
            case END:
                event.setCancelled( true );
                break;
        }
    }

    @EventHandler
    public void onEntityDamage( EntityDamageByEntityEvent event ) {
        if ( BedWarsPlayer.getBedWarsPlayer( event.getDamager().getUniqueId() ).isBuildMode() ) return;
        if ( event.getEntity() instanceof Villager || event.getEntity() instanceof Sheep ) {
            event.setCancelled( true );
            return;
        }
        if ( event.getEntity() instanceof Player && event.getDamager() instanceof Player ) {
            BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( event.getEntity().getUniqueId() );
            BedWarsPlayer bedWarsDamager = BedWarsPlayer.getBedWarsPlayer( event.getDamager().getUniqueId() );
            if ( bedWarsDamager.getTeam() == bedWarsPlayer.getTeam() ) event.setCancelled( true );
            else {
                bedWarsPlayer.setLastDamager( (Player) event.getDamager() );
                bedWarsPlayer.setLastDamagerTime( System.currentTimeMillis() );
            }
        }
    }

}
