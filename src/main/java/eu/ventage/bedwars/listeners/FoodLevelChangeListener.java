package eu.ventage.bedwars.listeners;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.gamestates.GameState;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodLevelChangeListener implements Listener {

    public FoodLevelChangeListener( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @EventHandler( priority = EventPriority.LOWEST )
    public void onFoodLevelChange( FoodLevelChangeEvent event ) {
        switch ( BedWars.getInstance().getGameState() ) {
            case LOBBY:
            case END:
                event.setFoodLevel( 20 );
                break;
        }
    }

}
