package eu.ventage.bedwars;

import eu.ventage.bedwars.commands.*;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.listeners.*;
import eu.ventage.bedwars.maps.MapManager;
import eu.ventage.bedwars.mysql.AsyncMySQL;
import eu.ventage.bedwars.runnable.LobbyCountdown;
import eu.ventage.bedwars.shop.ShopManager;
import eu.ventage.bedwars.shopitems.armor.*;
import eu.ventage.bedwars.shopitems.blocks.*;
import eu.ventage.bedwars.shopitems.bows.ArrowShopItem;
import eu.ventage.bedwars.shopitems.bows.BowLvl1ShopItem;
import eu.ventage.bedwars.shopitems.bows.BowLvl2ShopItem;
import eu.ventage.bedwars.shopitems.bows.BowLvl3ShopItem;
import eu.ventage.bedwars.shopitems.chests.ChestShopItem;
import eu.ventage.bedwars.shopitems.chests.EnderChestShopItem;
import eu.ventage.bedwars.shopitems.food.AppleShopItem;
import eu.ventage.bedwars.shopitems.food.CakeShopItem;
import eu.ventage.bedwars.shopitems.food.GoldAppleShopItem;
import eu.ventage.bedwars.shopitems.food.PorkShopItem;
import eu.ventage.bedwars.shopitems.pickaxes.IronpickaxeShopItem;
import eu.ventage.bedwars.shopitems.pickaxes.StonepickaxeShopItem;
import eu.ventage.bedwars.shopitems.pickaxes.WoodpickaxeShopItem;
import eu.ventage.bedwars.shopitems.potions.*;
import eu.ventage.bedwars.shopitems.special.*;
import eu.ventage.bedwars.shopitems.swords.KnockbackShopItem;
import eu.ventage.bedwars.shopitems.swords.SwordLvl1ShopItem;
import eu.ventage.bedwars.shopitems.swords.SwordLvl2ShopItem;
import eu.ventage.bedwars.shopitems.swords.SwordLvl3ShopItem;
import eu.ventage.bedwars.teams.TeamManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

@Getter
public class BedWars extends JavaPlugin {

    @Getter
    private static BedWars instance;
    private AsyncMySQL asyncMySQL;

    private TeamManager teamManager;
    private MapManager mapManager;
    private ShopManager shopManager;

    private LobbyCountdown lobbyCountdown;
    @Setter
    private GameState gameState;

    @Override
    public void onEnable( ) {
        instance = this;
        setUp();
    }

    @Override
    public void onDisable( ) {

    }

    private void setUp( ) {
        try {
            setUpConfig();
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        gameState = GameState.LOBBY;
        Data.blocks = new ArrayList<>();
        mapManager = new MapManager();
        teamManager = new TeamManager();
        shopManager = new ShopManager();
        lobbyCountdown = new LobbyCountdown( 60 );
        new PlayerInteractListener();
        new InventoryClickListener();
        new FoodLevelChangeListener();
        new EntityInteractListener();
        new BlockPlaceListener();
        new BlockBreakListener();
        new EntitySpawnListener();
        new PlayerDeathListener();
        new EntityDamageListener();
        new PlayerJoinListener();
        new PlayerQuitListener();
        new ServerListPingListener();
        new PlayerPickUpListener();
        new PlayerDropListener();
        new EntityExplosionListener();
        new AsyncPlayerChatListener();
        new WeatherChangeListener();
        this.getCommand( "shop" ).setExecutor( new ShopCommand() );
        this.getCommand( "start" ).setExecutor( new StartCommand() );
        this.getCommand( "setup" ).setExecutor( new SetUpCommand() );
        this.getCommand( "stats" ).setExecutor( new StatsCommand() );
        this.getCommand( "build" ).setExecutor( new BuildCommand() );
        this.getCommand( "goto" ).setExecutor( new GotoCommand() );
        setUpItems();
    }

    private void setUpConfig( ) throws IOException {
        getDataFolder().mkdirs();
        File file = new File( getDataFolder(), "config.yml" );
        if ( !file.exists() ) file.createNewFile();
        getConfig().addDefault( "MySQL.Host", "localhost" );
        getConfig().addDefault( "MySQL.Port", 3306 );
        getConfig().addDefault( "MySQL.Username", "root" );
        getConfig().addDefault( "MySQL.Password", "PASSWORD" );
        getConfig().addDefault( "MySQL.Database", "Bedwars" );
        getConfig().addDefault( "Maps", Arrays.asList( "world" ) );
        getConfig().options().copyDefaults( true );
        saveConfig();
        asyncMySQL = new AsyncMySQL( this, getConfig().getString( "MySQL.Host" ),
                getConfig().getInt( "MySQL.Port" ), getConfig().getString( "MySQL.Username" ),
                getConfig().getString( "MySQL.Password" ), getConfig().getString( "MySQL.Database" ) );
        for ( String maps : getConfig().getStringList( "Maps" ) ) {
            Bukkit.createWorld( new WorldCreator( maps ) );
        }
    }

    private void setUpItems( ) {
        // Blocks
        shopManager.registerShopItem( new SandBlockShopItem() );
        shopManager.registerShopItem( new SnowShopItem() );
        shopManager.registerShopItem( new IceBlockShopItem() );
        shopManager.registerShopItem( new GlowStoneShopItem() );
        shopManager.registerShopItem( new GlassShopItem() );
        shopManager.registerShopItem( new EndStoneShopItem() );

        // Armor
        shopManager.registerShopItem( new LeatherHelmetShopItem() );
        shopManager.registerShopItem( new LeatherLeggingsShopItem() );
        shopManager.registerShopItem( new LeatherBootsShopItem() );
        shopManager.registerShopItem( new ChestplateLvl1ShopItem() );
        shopManager.registerShopItem( new ChestplateLvl2ShopItem() );
        shopManager.registerShopItem( new ChestplateLvl3ShopItem() );

        // Combat
        shopManager.registerShopItem( new KnockbackShopItem() );
        shopManager.registerShopItem( new SwordLvl1ShopItem() );
        shopManager.registerShopItem( new SwordLvl2ShopItem() );
        shopManager.registerShopItem( new SwordLvl3ShopItem() );

        // Bows
        shopManager.registerShopItem( new BowLvl1ShopItem() );
        shopManager.registerShopItem( new BowLvl2ShopItem() );
        shopManager.registerShopItem( new BowLvl3ShopItem() );
        shopManager.registerShopItem( new ArrowShopItem() );

        // Chest
        shopManager.registerShopItem( new ChestShopItem() );
        shopManager.registerShopItem( new EnderChestShopItem() );

        // Food
        shopManager.registerShopItem( new AppleShopItem() );
        shopManager.registerShopItem( new PorkShopItem() );
        shopManager.registerShopItem( new CakeShopItem() );
        shopManager.registerShopItem( new GoldAppleShopItem() );

        // Pickaxe
        shopManager.registerShopItem( new WoodpickaxeShopItem() );
        shopManager.registerShopItem( new StonepickaxeShopItem() );
        shopManager.registerShopItem( new IronpickaxeShopItem() );

        // Potions
        shopManager.registerShopItem( new HealingLvl1ShopItem() );
        shopManager.registerShopItem( new HealingLvl2ShopItem() );
        shopManager.registerShopItem( new SwiftShopItem() );
        shopManager.registerShopItem( new StrengthShopItem() );
        shopManager.registerShopItem( new InvisibleShopItem() );

        // Special
        shopManager.registerShopItem( new LadderShopItem() );
        shopManager.registerShopItem( new CobWebShopItem() );
        shopManager.registerShopItem( new TNTSheepShopItem() );
        shopManager.registerShopItem( new FlintNSteelShopItem() );
        shopManager.registerShopItem( new TNTShopItem() );
        shopManager.registerShopItem( new TeleporterShopItem() );
        shopManager.registerShopItem( new RettungsplattformShopItem() );
        shopManager.registerShopItem( new EnderpearlShopItem() );
        shopManager.registerShopItem( new GravitationShopItem() );

    }

}
