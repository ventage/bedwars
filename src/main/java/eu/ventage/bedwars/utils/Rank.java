package eu.ventage.bedwars.utils;

import lombok.Getter;
import org.bukkit.entity.Player;

public enum Rank {
    ADMIN( "server.admin", "§4Admin", "§4", 1 ),
    DEV( "server.dev", "§bDev", "§b", 2 ),
    SRMOD( "server.srmod", "§cSrMod", "§c", 3 ),
    MOD( "server.mod", "§cMod", "§c", 4 ),
    SRBUILDER( "server.srbuilder", "§eSB", "§e", 5 ),
    BUILDER( "server.builder", "§eB", "§e", 6 ),
    YOUTUBER( "server.youtuber", "§5YT", "§5", 7 ),
    VIP( "server.vip", "§5VIP", "§5", 8 ),
    PREMIUM( "server.premium", "§6P", "§6", 9 ),
    SPIELER( "", "§7S", "§7", 10 );

    @Getter
    private String permission, prefix, color;
    @Getter
    private int order;

    Rank( String permission, String prefix, String color, int order ) {
        this.permission = permission;
        this.prefix = prefix;
        this.order = order;
        this.color = color;
    }

    public static Rank getTeamByString( String teamName ) {
        for ( Rank team : Rank.values() ) {
            if ( team.name().equalsIgnoreCase( teamName ) )
                return team;
        }
        return SPIELER;
    }

    public static Rank getTeamByPlayerPermission( Player player ) {
        for ( Rank team : Rank.values() ) {
            if ( player.hasPermission( team.getPermission() ) )
                return team;
        }
        return SPIELER;
    }

}
