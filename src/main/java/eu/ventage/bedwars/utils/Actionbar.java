package eu.ventage.bedwars.utils;

import eu.ventage.bedwars.BedWars;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Actionbar {

    @Getter
    @Setter
    private String text;
    @Getter
    private Player player;
    @Getter
    private int updateInterval;
    private PacketPlayOutChat packet;

    public Actionbar( Player player, String text, int updateInterval ) {
        this.text = text;
        this.player = player;
        this.updateInterval = updateInterval;
        update();
    }

    private void update( ) {
        Bukkit.getScheduler().runTaskTimerAsynchronously( BedWars.getInstance(), ( ) -> {
            this.packet = new PacketPlayOutChat( IChatBaseComponent.ChatSerializer.a( "{\"text\":\"" + text + "\"}" ), (byte) 2 );
            ( (CraftPlayer) player ).getHandle().playerConnection.sendPacket( packet );
        }, updateInterval, 20 );
    }

}
