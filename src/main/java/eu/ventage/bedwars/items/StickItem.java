package eu.ventage.bedwars.items;

import lombok.Getter;
import org.bukkit.Material;

public enum StickItem {

    STICK( Material.STICK ),
    RABBIT_FOOT( Material.RABBIT_FOOT ),
    BONE( Material.BONE );

    @Getter
    private Material material;

    StickItem( Material material ) {
        this.material = material;
    }


}
