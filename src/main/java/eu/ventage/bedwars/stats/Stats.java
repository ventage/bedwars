package eu.ventage.bedwars.stats;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.mysql.AsyncMySQL;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Stats {

    public static Stats getStats( UUID uuid ) {
        try {
            return statsCache.get( uuid );
        } catch ( ExecutionException e ) {
            e.printStackTrace();
        }
        return null;
    }

    private static final LoadingCache<UUID, Stats> statsCache = CacheBuilder.newBuilder().build( new CacheLoader<UUID, Stats>() {
        @Override
        public Stats load( UUID uuid ) throws Exception {
            return new Stats( uuid );
        }
    } );

    @Getter
    private UUID uuid;
    @Getter
    @Setter
    private int
            kills,
            deaths,
            playedGames,
            gamesWon,
            destroyedBeds,
            tempKills,
            tempBedsDestroyed;
    private AsyncMySQL asyncMySQL;

    private Stats( UUID uuid ) {
        this.uuid = uuid;
        this.tempKills = 0;
        this.tempBedsDestroyed = 0;
        this.asyncMySQL = BedWars.getInstance().getAsyncMySQL();
        try {
            setUpMySQL();
            loadMySQL();
        } catch ( SQLException e ) {
            e.printStackTrace();
        }
    }


    private void setUpMySQL( ) throws SQLException {
        asyncMySQL.getMySQL().queryUpdate( "CREATE TABLE IF NOT EXISTS Stats(UUID VARCHAR(36), KILLS INT, DEATHS INT, GAMESPLAYED INT, GAMESWON INT, BEDSDESTROYED INT);" );
        ResultSet query = asyncMySQL.getMySQL().query( "SELECT KILLS FROM Stats WHERE UUID='" + uuid + "'" );
        if ( !query.next() ) {
            PreparedStatement prepare = asyncMySQL.prepare( "INSERT INTO `Stats`(`UUID`, `KILLS`, `DEATHS`, `GAMESPLAYED`, `GAMESWON`, `BEDSDESTROYED`) VALUES (?,?,?,?,?,?)" );
            prepare.setString( 1, uuid.toString() );
            prepare.setInt( 2, 0 );
            prepare.setInt( 3, 0 );
            prepare.setInt( 4, 0 );
            prepare.setInt( 5, 0 );
            prepare.setInt( 6, 0 );
            asyncMySQL.getMySQL().queryUpdate( prepare );
            query.close();
        }

    }

    private void loadMySQL( ) throws SQLException {
        PreparedStatement prepare = asyncMySQL.prepare( "SELECT * FROM Stats WHERE UUID= ?" );
        prepare.setString( 1, uuid.toString() );
        ResultSet query = asyncMySQL.getMySQL().query( prepare );
        boolean next = query.next();
        System.out.println( next );
        if ( next ) {
            kills = query.getInt( "KILLS" );
            deaths = query.getInt( "DEATHS" );
            playedGames = query.getInt( "GAMESPLAYED" );
            gamesWon = query.getInt( "GAMESWON" );
            destroyedBeds = query.getInt( "BEDSDESTROYED" );
            query.close();
        }
    }

    public int addKills( int added ) {
        return kills = kills + added;
    }

    public int removeKills( int removed ) {
        return kills = kills - removed;
    }

    public int addDeaths( int added ) {
        return deaths = deaths + added;
    }

    public int removeDeaths( int removed ) {
        return deaths = deaths - removed;
    }

    public int addPlayedGames( int added ) {
        return playedGames = playedGames + added;
    }

    public int removePlayedGames( int removed ) {
        return playedGames = playedGames - removed;
    }

    public int addGamesWon( int added ) {
        return gamesWon = gamesWon + added;
    }

    public int removeGamesWon( int removed ) {
        return gamesWon = gamesWon - removed;
    }

    public int addDestroyedBeds( int added ) {
        return destroyedBeds = destroyedBeds + added;
    }

    public int removeDestroyedBeds( int removed ) {
        return destroyedBeds = destroyedBeds - removed;
    }

    public int getRank( ) throws SQLException {
        PreparedStatement prepare = asyncMySQL.prepare( "SELECT * FROM `Stats` WHERE 1 ORDER BY `Stats`.`GAMESWON` DESC" );
        int i = 0;
        ResultSet query = asyncMySQL.getMySQL().query( prepare );
        while ( query.next() ) {
            i++;
            if ( query.getString( "UUID" ).equals( uuid.toString() ) ) {
                query.close();
                return i;
            }
        }
        return i;
    }

    public double getKD( ) {
        return (double) kills / deaths;
    }

    public double getWinPercentage( ) {
        if ( gamesWon == 0 || playedGames == 0 ) return 0;
        return ( gamesWon * 100 ) / playedGames;
    }

    public void addTempKills( int i ) {
        this.tempKills = tempKills + i;
    }

    public void addTempBedsDestroyed( int i ) {
        this.tempBedsDestroyed = tempBedsDestroyed + i;
    }

    public void save( ) {
        addKills( getTempKills() );
        addDestroyedBeds( getTempBedsDestroyed() );
        Player player = Bukkit.getPlayer( uuid );
        if ( player != null )
            player.sendMessage( Data.prefix + "§7Deine Stats wurden §a§ogespeichert" );
        PreparedStatement prepare = asyncMySQL.prepare( "UPDATE Stats SET KILLS= ?, DEATHS= ?, GAMESPLAYED= ?, GAMESWON= ?, BEDSDESTROYED= ? WHERE UUID= ?" );
        try {
            prepare.setInt( 1, kills );
            prepare.setInt( 2, deaths );
            prepare.setInt( 3, playedGames );
            prepare.setInt( 4, gamesWon );
            prepare.setInt( 5, destroyedBeds );
            prepare.setString( 6, uuid.toString() );
            BedWars.getInstance().getAsyncMySQL().update( prepare );
        } catch ( SQLException e ) {
            e.printStackTrace();
        }

    }

}
