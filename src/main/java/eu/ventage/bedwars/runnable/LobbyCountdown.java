package eu.ventage.bedwars.runnable;

import de.dytanic.cloudnet.bridge.CloudServer;
import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.entity.EntityBuilder;
import eu.ventage.bedwars.gamestates.GameState;
import eu.ventage.bedwars.maps.Map;
import eu.ventage.bedwars.maps.MapManager;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.player.ScoreboardManager;
import eu.ventage.bedwars.teams.Team;
import eu.ventage.bedwars.teams.TeamManager;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.*;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class LobbyCountdown {

    @Getter
    @Setter
    private int now;
    private int time;
    @Getter
    private BukkitTask bukkitTask;

    public LobbyCountdown( int time ) {
        this.time = time;
        this.now = time;
    }

    public void start( ) {
        bukkitTask = Bukkit.getScheduler().runTaskTimer( BedWars.getInstance(), new Runnable() {
            @Override
            public void run( ) {
                now--;
                if ( now == 0 ) bukkitTask.cancel();
                for ( Player player : Bukkit.getOnlinePlayers() ) {
                    player.setLevel( now );
                    player.setExp( (float) now / time );
                }
                if ( now <= 5 && now != 0 || now == 10 || now == 15 || now == 30 || now == 60 ) {
                    Bukkit.broadcastMessage( Data.prefix + "§7Das Spiel startet in §e" + now + " §7Sekunden§8." );
                    for ( Player player : Bukkit.getOnlinePlayers() )
                        player.playSound( player.getLocation(), Sound.NOTE_PLING, 1, 1 );
                }
                if ( now == 0 ) {
                    CloudServer.getInstance().changeToIngame();
                    Bukkit.broadcastMessage( Data.prefix + "§7Das Spiel startet in jetzt§8." );
                    BedWars.getInstance().setGameState( GameState.INGAME );
                    MapManager mapManager = BedWars.getInstance().getMapManager();
                    mapManager.startSpawning();
                    Map map = mapManager.getActiveMap();
                    for ( Location location : map.getVillager() )
                        if ( location != null )
                            new EntityBuilder( EntityType.VILLAGER ).setNoAI( true ).setNBTTag( "Silent", "1" ).spawn( location );
                    TeamManager teamManager = BedWars.getInstance().getTeamManager();
                    map.getWorld().setTime( 0 );
                    map.getWorld().setWeatherDuration( 0 );
                    for ( Player player : Bukkit.getOnlinePlayers() ) {
                        BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
                        if ( bedWarsPlayer.getTeam() == -1 ) {
                            HashMap<Team, Double> teams = new HashMap<>();
                            for ( Team team : teamManager.getTeams() )
                                teams.put( team, Double.valueOf( team.getTeam() + "." + teamManager.getPlayers( team ).size() ) );
                            Double min1 = Collections.min( teams.values() );
                            String[] min = min1.toString().split( "\\." );
                            int players = Integer.parseInt( min[0] );
                            Team team = teamManager.getTeams().get( Integer.valueOf( min[1] ) );
                            System.out.println( team.getName() );
                            if ( players < ( map.getTeamSizes().getTeams() / map.getTeamSizes().getServerSize() ) ) {
                                player.kickPlayer( "§7Alle Teams sind voll §4RIP" );
                            } else {
                                System.out.println( players + " " + map.getTeamSizes().getTeams() / map.getTeamSizes().getServerSize() );
                                teamManager.addPlayer( team, player );
                            }
                        }
                    }
                    for ( Team team : teamManager.getTeams() ) {
                        List<Player> players = teamManager.getPlayers( team );
                        if ( players.size() < 1 ) teamManager.removeBed( team );
                        for ( Player player : players ) {
                            player.setBedSpawnLocation( map.getTeams().get( team.getTeam() - 1 ), true );
                            player.teleport( map.getTeams().get( team.getTeam() - 1 ) );
                        }
                    }
                    BedWars.getInstance().getMapManager().getActiveMap().getTeams();
                    for ( Player player : Bukkit.getOnlinePlayers() ) {
                        player.playSound( player.getLocation(), Sound.LEVEL_UP, 1, 1 );
                        player.closeInventory();
                        player.setGameMode( GameMode.SURVIVAL );
                        ScoreboardManager scoreboardManager = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() ).getScoreboardManager();
                        scoreboardManager.update();
                        scoreboardManager.updateSideboard();
                        player.getInventory().clear();
                    }
                }
            }
        }, 1, 20 );
    }
}
