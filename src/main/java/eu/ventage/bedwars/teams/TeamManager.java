package eu.ventage.bedwars.teams;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.maps.MapManager;
import eu.ventage.bedwars.player.BedWarsPlayer;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeamManager {

    private static HashMap<Team, List<Player>> teams;
    @Getter
    private static List<Team> beds;

    public TeamManager( ) {
        teams = new HashMap<>();
        beds = new ArrayList<>();
        registerTeams();
    }

    public void registerTeams( ) {
        int i = 0;
        MapManager mapManager = BedWars.getInstance().getMapManager();
        for ( Team team : Team.values() ) {
            if ( mapManager.getActiveMap().getTeamSizes().getTeams() > i ) {
                i++;
                teams.put( team, new ArrayList<>() );
                beds.add( team );
            }
        }
    }

    public boolean hasBed( Team team ) {
        return beds.contains( team );
    }

    public static List<Player> getPlayersFromTeam( Team team ) {
        return teams.get( team );
    }

    public void addPlayer( Team team, Player player ) {
        BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
        bedWarsPlayer.setTeam( team.getTeam() );
        removePlayer( player );
        teams.get( team ).add( player );
        bedWarsPlayer.getActionbar().setText( team.getColor() + "Team " + team.getName() );
    }

    public void removePlayer( Player player ) {
        teams.forEach( ( team, players ) -> {
            if ( players.contains( player ) )
                players.remove( player );
        } );
    }

    public static boolean hasTeam( Player player ) {
        for ( List<Player> players : teams.values() )
            if ( players.contains( player ) )
                return true;
        return false;
    }

    public static Team getTeam( Player player ) {
        for ( Team team : Team.values() )
            if ( teams.get( team ).contains( player ) )
                return team;
        return null;
    }

    public List<Player> getPlayers( Team team ) {
        return teams.get( team );
    }

    public List<Team> getTeams( ) {
        List<Team> teamss = new ArrayList<>();
        for ( Team team : teams.keySet() )
            teamss.add( team );
        return teamss;
    }

    public void removeBed( Team team ) {
        beds.remove( team );
    }
}
