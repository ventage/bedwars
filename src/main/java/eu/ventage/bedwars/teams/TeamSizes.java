package eu.ventage.bedwars.teams;

import lombok.Getter;

public enum TeamSizes {

    T8x1( 8, 8, 3 ),
    T8x2( 16, 8, 4 ),
    T4x1( 4, 4, 2 ),
    T4x2( 8, 4, 2 ),
    T2x2( 4, 2, 2 );

    @Getter
    private int serverSize, teams, playersToStart;

    TeamSizes( int serverSize, int teams, int playersToStart ) {
        this.serverSize = serverSize;
        this.teams = teams;
        this.playersToStart = playersToStart;
    }

    public static TeamSizes getTeamSizeByString( String name ) {
        for ( TeamSizes teamSizes : TeamSizes.values() ) {
            if ( teamSizes.name().equalsIgnoreCase( name ) )
                return teamSizes;
        }
        return null;
    }

}
