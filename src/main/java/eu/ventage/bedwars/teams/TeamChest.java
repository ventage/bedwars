package eu.ventage.bedwars.teams;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class TeamChest {


    @Getter
    private Team team;
    @Getter
    private Inventory inventory;

    public TeamChest( Team team ) {
        this.team = team;
        this.inventory = Bukkit.createInventory( null, 3 * 9, team.getColor() + "Team " + team.getName() );
    }

    public void open( Player player ) {
        player.openInventory( inventory );
    }

}
