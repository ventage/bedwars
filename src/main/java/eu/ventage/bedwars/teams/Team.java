package eu.ventage.bedwars.teams;

import lombok.Getter;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.material.Dye;

public enum Team {

    RED( "Rot", "§c", 1, 14, Color.RED, DyeColor.RED ),
    BLUE( "Blau", "§9", 2, 11, Color.BLUE, DyeColor.BLUE ),
    GREEN( "Grün", "§a", 3, 5, Color.GREEN, DyeColor.GREEN ),
    YELLOW( "Gelb", "§e", 4, 4, Color.YELLOW, DyeColor.YELLOW ),
    PURPLE( "Lila", "§5", 5, 10, Color.PURPLE, DyeColor.PURPLE ),
    AQUA( "Türkis", "§b", 6, 3, Color.AQUA, DyeColor.CYAN ),
    ORANGE( "Orange", "§6", 7, 1, Color.ORANGE, DyeColor.ORANGE ),
    PINK( "Pink", "§d", 8, 6, Color.FUCHSIA, DyeColor.PINK );

    @Getter
    private String name, color;
    @Getter
    private int team, woolID;
    @Getter
    private Color leatherColor;
    @Getter
    private TeamChest teamChest;
    @Getter
    private DyeColor sheepColor;

    Team( String name, String color, int team, int woolID, Color leatherColor, DyeColor sheepColor ) {
        this.name = name;
        this.color = color;
        this.team = team;
        this.woolID = woolID;
        this.leatherColor = leatherColor;
        this.sheepColor = sheepColor;
        this.teamChest = new TeamChest( this );
    }

    public static Team getTeamByNumber( int team ) {
        if ( Team.values().length < team ) return null;
        for ( Team team1 : Team.values() )
            if ( team1.team == team )
                return team1;
        return null;
    }

    public static Team getTeamByString( String string ) {
        for ( Team team1 : Team.values() )
            if ( team1.name().equalsIgnoreCase( string )
                    || team1.getName().equalsIgnoreCase( string ) )
                return team1;
        return null;
    }

    public static Team getTeamByWoolID( int subID ) {
        for ( Team team : Team.values() ) {
            if ( team.woolID == subID ) {
                return team;
            }
        }
        return null;
    }


}
