package eu.ventage.bedwars.gamestates;

public enum GameState {

    LOBBY,
    INGAME,
    END

}
