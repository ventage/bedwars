package eu.ventage.bedwars.inventory;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.items.ItemBuilder;
import eu.ventage.bedwars.merchant.Merchant;
import eu.ventage.bedwars.merchant.MerchantOffer;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shop.Shop;
import eu.ventage.bedwars.shop.ShopManager;
import eu.ventage.bedwars.shopitems.*;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class ShopInventory {

    @Getter
    private BedWarsPlayer bedWarsPlayer;

    public ShopInventory( BedWarsPlayer bedWarsPlayer ) {
        this.bedWarsPlayer = bedWarsPlayer;
    }

    public void openMainInv( ) {
        Inventory inventory = Bukkit.createInventory( null, 3 * 9, "§aShop" );
        int i = 9;
        for ( Category category : Category.values() ) {
            inventory.setItem( i++, new ItemBuilder( category.getMaterial() ).setDisplayName( category.getName() ).build() );
        }
        bedWarsPlayer.getPlayer().openInventory( inventory );
    }

    public void enterSubTrade( Category category ) {
        ShopManager shopManager = BedWars.getInstance().getShopManager();
        List<ShopItem> items = shopManager.getItemsFromCategory( category );
        if ( bedWarsPlayer.getShop().equals( Shop.OLD ) ) {
            List<MerchantOffer> offers = new ArrayList<>();
            for ( ShopItem item : items ) {
                if ( item.category().equals( category ) )
                    offers.add( new MerchantOffer( createItemFromShopItem( false, item ), null, createItemFromShopItem( true, item ) ) );
            }
            Merchant merchant = new Merchant();
            for ( MerchantOffer offer : offers ) merchant.addOffer( offer );
            merchant.setTitle( category.getName() );
            merchant.openTrading( bedWarsPlayer.getPlayer() );
            return;
        }
        Inventory inventory = Bukkit.createInventory( null, 6 * 9, category.getName() );
        int i = 9, o = 9;
        for ( ShopItem item : items ) {
            if ( item.category().equals( category ) ) {
                if ( i - o > 6 ) i = o + 18;
                i++;
                inventory.setItem( i, createItemFromShopItem( true, item ) );
                inventory.setItem( i + 9, createItemFromShopItem( false, item ) );
            }
        }
        inventory.setItem( 53, new ItemBuilder( Material.BARRIER ).setDisplayName( "§4Zurück" ).build() );
        bedWarsPlayer.getPlayer().openInventory( inventory );
    }

    private ItemStack createItemFromShopItem( boolean first, ShopItem item ) {
        if ( first ) {
            ItemBuilder itemBuilder = new ItemBuilder( item.material() );
            itemBuilder.setDisplayName( item.displayName() ).setSubID( item.subID() ).setAmount( item.amount() ).setLore( item.lore() );
            if ( item instanceof ChangableShopItem )
                itemBuilder.setMaterial( ( (ChangableShopItem) item ).material( bedWarsPlayer ) );
            if ( item instanceof ColoredShopItem )
                itemBuilder.setLeatherColor( ( (ColoredShopItem) item ).color( bedWarsPlayer ) );
            if ( item instanceof EnchantedShopItem )
                ( (EnchantedShopItem) item ).enchantmentList().forEach( ( enchantment, integer ) -> itemBuilder.enchant( enchantment, integer ) );
            return itemBuilder.build();
        }
        ItemBuilder itemBuilder = new ItemBuilder( item.priceItem().getMaterial() );
        itemBuilder.setDisplayName( item.priceItem().getName() ).setAmount( item.price() );
        return itemBuilder.build();
    }


}
