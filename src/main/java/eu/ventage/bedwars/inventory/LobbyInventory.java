package eu.ventage.bedwars.inventory;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.items.ItemBuilder;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shop.Shop;
import eu.ventage.bedwars.stats.Stats;
import eu.ventage.bedwars.teams.Team;
import eu.ventage.bedwars.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class LobbyInventory {

    public static void setInventory( Player player ) {
        Inventory inventory = player.getInventory();
        inventory.clear();
        inventory.setItem( 0, new ItemBuilder( Material.BED ).setDisplayName( "§aTeam Auswahl" ).build() );
        inventory.setItem( 2, new ItemBuilder( Material.NETHER_STAR ).setDisplayName( "§9Stats" ).build() );
        inventory.setItem( 4, new ItemBuilder( Material.MONSTER_EGG ).setDisplayName( "§eShop Einstellungen" ).setSubID( 120 ).build() );
        inventory.setItem( 8, new ItemBuilder( Material.SKULL_ITEM ).setHeadOwner( "MHF_ARROWRIGHT" ).setSubID( 3 ).setDisplayName( "§cVerlassen" ).build() );
    }

    public static void setTeamInventory( Player player ) {
        Inventory inventory = Bukkit.createInventory( null, 1 * 9, "§aTeam Auswahl" );
        player.openInventory( inventory );
        System.out.println( BedWars.getInstance().getMapManager().getActiveMap().getTeamSizes().getTeams() );
        for ( int i = 0; i < BedWars.getInstance().getMapManager().getActiveMap().getTeamSizes().getTeams(); i++ ) {
            Team team = Team.getTeamByNumber( i + 1 );
            List<Player> players = BedWars.getInstance().getTeamManager().getPlayers( team );
            String playerString = "";
            for ( Player player1 : players )
                playerString = playerString + Rank.getTeamByPlayerPermission( player1 ).getColor() + player1.getName() + ", ";
            String[] playerArray = playerString.split( ", " );
            ItemStack item = new ItemBuilder( Material.WOOL ).setDisplayName( team.getColor() + team.getName() )
                    .setLore( playerArray ).setSubID( team.getWoolID() ).setAmount( players.size() > 0 ? players.size() : 1 ).build();
            inventory.addItem( item );
            player.openInventory( inventory );
        }
    }

    public static void setShopEinstellungen( Player player ) {
        Inventory inventory = Bukkit.createInventory( null, 3 * 9, "§eShop Einstellungen" );
        BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
        Shop active = bedWarsPlayer.getShop();
        ItemStack oldShop = new ItemBuilder( Material.MONSTER_EGG ).setDisplayName( ( active == Shop.OLD ? "§a" : "§c" ) + Shop.OLD.getName() ).setSubID( 120 ).build();
        ItemStack head = new ItemBuilder( Material.SKULL_ITEM )
                .setHeadOwner( active == Shop.OLD ? "MHF_ARROWLEFT" : "MHF_ARROWRIGHT" ).setSubID( 3 ).setDisplayName( "§8" + ( active == Shop.OLD ? "«" : "»" ) ).build();
        ItemStack newShop = new ItemBuilder( Material.BED ).setDisplayName( ( active == Shop.NEW ? "§a" : "§c" ) + Shop.NEW.getName() ).build();
        inventory.setItem( 9 + 2, oldShop );
        inventory.setItem( 9 + 4, head );
        inventory.setItem( 9 + 6, newShop );
        player.openInventory( inventory );
    }

    public static void setStatsInventory( Player player ) {
        Inventory inventory = Bukkit.createInventory( null, 1 * 9, "§9Stats" );
        Stats stats = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() ).getStats();

        inventory.setItem( 4, new ItemBuilder( Material.SKULL_ITEM )
                .setDisplayName( "§aStats von " + Rank.getTeamByPlayerPermission( player ).getColor() + player.getName() )
                .setSubID( 3 ).setHeadOwner( player.getName() ).build() );

        inventory.setItem( 0, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aK/D: §e" + stats.getKD() ).build() );
        inventory.setItem( 1, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aKills: §e" + stats.getKills() ).build() );
        inventory.setItem( 2, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aDeahts: §e" + stats.getDeaths() ).build() );
        inventory.setItem( 6, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aGewinnProzent: §e" + stats.getWinPercentage() + "%" ).build() );
        inventory.setItem( 7, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aSpiele Gewonnen: §e" + stats.getGamesWon() ).build() );
        inventory.setItem( 8, new ItemBuilder( Material.PAPER )
                .setDisplayName( "§aSpiele Gespielt: §e" + stats.getPlayedGames() ).build() );

        player.openInventory( inventory );
    }
}
