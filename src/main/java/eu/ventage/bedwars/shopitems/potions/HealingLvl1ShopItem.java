package eu.ventage.bedwars.shopitems.potions;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;

public class HealingLvl1ShopItem extends ShopItem {
    @Override
    public String displayName( ) {
        return "§aHeiltrank Lvl. 1";
    }

    @Override
    public Material material( ) {
        return Material.POTION;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 8197;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.IRON;
    }

    @Override
    public int price( ) {
        return 3;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.POTIONS;
    }
}
