package eu.ventage.bedwars.shopitems.potions;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;

public class StrengthShopItem extends ShopItem {
    @Override
    public String displayName( ) {
        return "§aStärketrank";
    }

    @Override
    public Material material( ) {
        return Material.POTION;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 8201;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.GOLD;
    }

    @Override
    public int price( ) {
        return 7;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.POTIONS;
    }
}
