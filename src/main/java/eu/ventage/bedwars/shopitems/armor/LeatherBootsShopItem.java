package eu.ventage.bedwars.shopitems.armor;

import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shopitems.*;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class LeatherBootsShopItem extends ShopItem implements ColoredShopItem, EnchantedShopItem {

    @Override
    public Color color( BedWarsPlayer bedWarsPlayer ) {
        return Team.getTeamByNumber( bedWarsPlayer.getTeam() ).getLeatherColor();
    }

    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.PROTECTION_ENVIRONMENTAL, 1 );
        hashMap.put( Enchantment.DURABILITY, 1 );
        return hashMap;
    }

    @Override
    public String displayName( ) {
        return "§aLederhelm";
    }

    @Override
    public Material material( ) {
        return Material.LEATHER_BOOTS;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 1;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.ARMOR;
    }
}
