package eu.ventage.bedwars.shopitems.armor;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.EnchantedShopItem;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class ChestplateLvl3ShopItem extends ShopItem implements EnchantedShopItem {
    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.PROTECTION_ENVIRONMENTAL, 3 );
        hashMap.put( Enchantment.DURABILITY, 2 );
        return hashMap;
    }

    @Override
    public String displayName( ) {
        return "§aChestplate Lvl. 3";
    }

    @Override
    public Material material( ) {
        return Material.CHAINMAIL_CHESTPLATE;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.IRON;
    }

    @Override
    public int price( ) {
        return 7;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.ARMOR;
    }
}
