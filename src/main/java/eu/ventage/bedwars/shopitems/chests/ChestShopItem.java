package eu.ventage.bedwars.shopitems.chests;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;

public class ChestShopItem extends ShopItem {

    @Override
    public String displayName( ) {
        return "";
    }

    @Override
    public Material material( ) {
        return Material.CHEST;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.IRON;
    }

    @Override
    public int price( ) {
        return 1;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.CHESTS;
    }
}
