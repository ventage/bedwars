package eu.ventage.bedwars.shopitems.swords;

import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shopitems.*;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class KnockbackShopItem extends ShopItem implements ChangableShopItem, EnchantedShopItem {
    @Override
    public String displayName( ) {
        return "";
    }

    @Override
    public Material material( ) {
        return Material.STICK;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 8;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.COMBAT;
    }

    @Override
    public Material material( BedWarsPlayer bedWarsPlayer ) {
        return bedWarsPlayer.getStickItem().getMaterial();
    }

    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.KNOCKBACK, 2 );
        return hashMap;
    }
}
