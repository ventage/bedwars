package eu.ventage.bedwars.shopitems.swords;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.EnchantedShopItem;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class SwordLvl2ShopItem extends ShopItem implements EnchantedShopItem {
    @Override
    public String displayName( ) {
        return "§aGoldschwert Lvl. 2";
    }

    @Override
    public Material material( ) {
        return Material.GOLD_SWORD;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.IRON;
    }

    @Override
    public int price( ) {
        return 3;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.COMBAT;
    }

    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.DAMAGE_ALL, 2 );
        hashMap.put( Enchantment.DURABILITY, 1 );
        return hashMap;
    }
}
