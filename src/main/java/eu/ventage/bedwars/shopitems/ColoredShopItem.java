package eu.ventage.bedwars.shopitems;

import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.Color;

public interface ColoredShopItem {

    public abstract Color color( BedWarsPlayer bedWarsPlayer );

}
