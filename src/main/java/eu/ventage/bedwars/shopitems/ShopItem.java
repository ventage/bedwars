package eu.ventage.bedwars.shopitems;

import org.bukkit.Material;

public abstract class ShopItem {

    public abstract String displayName( );

    public abstract Material material( );

    public abstract int amount( );

    public abstract int subID( );

    public abstract PriceItem priceItem( );

    public abstract int price( );

    public abstract String[] lore( );

    public abstract Category category( );

}
