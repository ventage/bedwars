package eu.ventage.bedwars.shopitems.blocks;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;

public class IceBlockShopItem extends ShopItem {
    @Override
    public String displayName( ) {
        return "Eisblock";
    }

    @Override
    public Material material( ) {
        return Material.PACKED_ICE;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 2;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.BLOCKS;
    }
}
