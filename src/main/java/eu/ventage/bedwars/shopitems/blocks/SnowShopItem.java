package eu.ventage.bedwars.shopitems.blocks;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public class SnowShopItem extends ShopItem implements Listener {

    public SnowShopItem( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @Override
    public String displayName( ) {
        return "";
    }

    @Override
    public Material material( ) {
        return Material.SNOW_BLOCK;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 2;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.BLOCKS;
    }

    @EventHandler
    public void onEvent( BlockPlaceEvent event ) {
        if ( event.getBlock().getType().equals( material() ) ) {
            Bukkit.getScheduler().runTaskLater( BedWars.getInstance(), ( ) -> {
                event.getBlock().setType( Material.AIR );
            }, 20 * 5 );
        }
    }
}
