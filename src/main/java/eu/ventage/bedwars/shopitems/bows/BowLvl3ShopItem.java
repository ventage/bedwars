package eu.ventage.bedwars.shopitems.bows;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.EnchantedShopItem;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class BowLvl3ShopItem extends ShopItem implements EnchantedShopItem {
    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.ARROW_INFINITE, 1 );
        hashMap.put( Enchantment.ARROW_DAMAGE, 1 );
        hashMap.put( Enchantment.ARROW_KNOCKBACK, 1 );
        return hashMap;
    }

    @Override
    public String displayName( ) {
        return "§aBogen Lvl. 3";
    }

    @Override
    public Material material( ) {
        return Material.BOW;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.GOLD;
    }

    @Override
    public int price( ) {
        return 13;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.BOWS;
    }
}
