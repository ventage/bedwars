package eu.ventage.bedwars.shopitems;

import lombok.Getter;
import org.bukkit.Material;

public enum Category {

    BLOCKS( Material.SANDSTONE, "§aBlöcke" ),
    ARMOR( Material.CHAINMAIL_CHESTPLATE, "§aRüstung" ),
    PICKAXES( Material.GOLD_PICKAXE, "§aSpitzhacken" ),
    COMBAT( Material.GOLD_SWORD, "§aSchwerter" ),
    BOWS( Material.BOW, "§aBögen" ),
    FOOD( Material.COOKED_BEEF, "§aEssen" ),
    POTIONS( Material.POTION, "§aTränke" ),
    CHESTS( Material.CHEST, "§aKisten" ),
    SPECIAL( Material.GOLDEN_APPLE, "§aSpezial" );


    @Getter
    private Material material;
    @Getter
    private String name;

    Category( Material material, String name ) {
        this.material = material;
        this.name = name;
    }

}
