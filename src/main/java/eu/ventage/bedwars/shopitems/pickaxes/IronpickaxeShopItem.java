package eu.ventage.bedwars.shopitems.pickaxes;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.EnchantedShopItem;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public class IronpickaxeShopItem extends ShopItem implements EnchantedShopItem {
    @Override
    public String displayName( ) {
        return "§aEisenspitzhacke";
    }

    @Override
    public Material material( ) {
        return Material.IRON_PICKAXE;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.GOLD;
    }

    @Override
    public int price( ) {
        return 1;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.PICKAXES;
    }

    @Override
    public HashMap<Enchantment, Integer> enchantmentList( ) {
        HashMap<Enchantment, Integer> hashMap = new HashMap<>();
        hashMap.put( Enchantment.DIG_SPEED, 3 );
        hashMap.put( Enchantment.DURABILITY, 1 );
        return hashMap;
    }

}
