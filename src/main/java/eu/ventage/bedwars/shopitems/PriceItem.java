package eu.ventage.bedwars.shopitems;

import lombok.Getter;
import org.bukkit.Material;

public enum PriceItem {

    BRONZE( Material.CLAY_BRICK, "§cBronze" ),
    IRON( Material.IRON_INGOT, "§7Eisen" ),
    GOLD( Material.GOLD_INGOT, "§6Gold" );

    @Getter
    private Material material;
    @Getter
    private String name;

    PriceItem( Material material, String name ) {
        this.material = material;
        this.name = name;
    }

}
