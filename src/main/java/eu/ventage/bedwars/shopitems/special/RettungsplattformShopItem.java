package eu.ventage.bedwars.shopitems.special;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class RettungsplattformShopItem extends ShopItem implements Listener {

    public RettungsplattformShopItem( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @Override
    public String displayName( ) {
        return "§aRettungsplattform";
    }

    @Override
    public Material material( ) {
        return Material.BLAZE_ROD;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.GOLD;
    }

    @Override
    public int price( ) {
        return 4;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.SPECIAL;
    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {
        if ( event.getAction().equals( Action.RIGHT_CLICK_AIR ) || event.getAction().equals( Action.RIGHT_CLICK_BLOCK ) ) {
            if ( event.getMaterial().equals( material() ) ) {
                ItemStack itemInHand = event.getPlayer().getInventory().getItemInHand();
                itemInHand.setAmount( itemInHand.getAmount() - 1 );
                event.getPlayer().setItemInHand( itemInHand );
                BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() );
                Location location = event.getPlayer().getLocation();
                location.getBlock().setType( Material.WEB );
                Data.blocks.add( location.getBlock() );
                Block block = location.subtract( 0, 1, 0 ).getBlock();
                setTeamBlock( bedWarsPlayer, block );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.NORTH ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.WEST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.SOUTH ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.EAST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.NORTH_EAST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.NORTH_WEST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.SOUTH_EAST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.SOUTH_WEST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.SOUTH ).getRelative( BlockFace.SOUTH ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.WEST ).getRelative( BlockFace.WEST ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.NORTH ).getRelative( BlockFace.NORTH ) );
                setTeamBlock( bedWarsPlayer, block.getRelative( BlockFace.EAST ).getRelative( BlockFace.EAST ) );
            }
        }
    }

    private void setTeamBlock( BedWarsPlayer bedWarsPlayer, Block block ) {
        if ( !block.getType().equals( Material.AIR ) ) return;
        Team team = Team.getTeamByNumber( bedWarsPlayer.getTeam() );
        block.setType( Material.STAINED_GLASS );
        block.setData( (byte) team.getWoolID() );
        Data.blocks.add( block );
    }

}
