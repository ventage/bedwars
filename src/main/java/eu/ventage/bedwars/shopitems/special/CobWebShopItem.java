package eu.ventage.bedwars.shopitems.special;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Material;

public class CobWebShopItem extends ShopItem {
    @Override
    public String displayName( ) {
        return "";
    }

    @Override
    public Material material( ) {
        return Material.WEB;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 16;
    }

    @Override
    public String[] lore( ) {
        return new String[]{ "§aLet's go! Schwitzer Style" };
    }

    @Override
    public Category category( ) {
        return Category.SPECIAL;
    }
}
