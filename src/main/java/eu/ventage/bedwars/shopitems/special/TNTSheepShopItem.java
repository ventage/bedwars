package eu.ventage.bedwars.shopitems.special;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.entity.TNTSheep;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import eu.ventage.bedwars.teams.Team;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TNTSheepShopItem extends ShopItem implements Listener {

    private HashMap<Location, Object[]> hashMap;
    private List<TNTSheep> sheeps;

    public TNTSheepShopItem( ) {
        hashMap = new HashMap<>();
        sheeps = new ArrayList<>();
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @Override
    public String displayName( ) {
        return "§aTNT-Schaf";
    }

    @Override
    public Material material( ) {
        return Material.MONSTER_EGG;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 91;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 64;
    }

    @Override
    public String[] lore( ) {
        return new String[]{ "§cComming Soon" };
    }

    @Override
    public Category category( ) {
        return Category.SPECIAL;
    }

    @EventHandler
    public void onSpawn( EntitySpawnEvent event ) {
        if ( !event.getEntityType().equals( EntityType.SHEEP ) ) return;
        Location location = event.getLocation();
        Bukkit.broadcastMessage( "EntitySpawn: " + location.toString() );
        location.setPitch( 0 );
        location.setYaw( 0 );
        if ( !hashMap.containsKey( location ) ) {
            event.setCancelled( true );
            return;
        }
        Object[] objects = hashMap.get( location );
        String playerName = (String) objects[0];
        int teamID = (Integer) objects[1];
        Sheep sheep = (Sheep) event.getEntity();
        sheep.setCustomName( playerName + ";" + teamID );
        sheep.setCustomNameVisible( false );
        Bukkit.broadcastMessage( sheep.getCustomName() );
        sheep.setSheared( false );
        sheep.setAdult();
        sheep.setColor( Team.getTeamByNumber( teamID ).getSheepColor() );
        TNTSheep tntSheep = new TNTSheep( sheep, playerName );
        List<Entity> nearbyEntities = sheep.getNearbyEntities( 50, 50, 50 );
        for ( Entity entity : nearbyEntities ) {
            if ( !entity.getType().equals( EntityType.PLAYER ) )
                continue;
            BedWarsPlayer player = BedWarsPlayer.getBedWarsPlayer( entity.getUniqueId() );
            if ( player.getTeam() == teamID )
                continue;
            tntSheep.setPath( entity.getLocation() );
            break;
        }
        sheeps.add( tntSheep );
        Bukkit.getScheduler().runTaskLater( BedWars.getInstance(), ( ) -> {
            tntSheep.explode();
            sheeps.remove( tntSheep );
        }, 20 * 10 );
    }

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {
        if ( event.getItem() == null ) return;
        if ( !event.getItem().getType().equals( Material.MONSTER_EGG ) ) return;
        if ( event.getItem().getData().getData() != 91 ) return;
        if ( !event.getAction().equals( Action.RIGHT_CLICK_BLOCK ) ) return;
        hashMap.put( event.getClickedBlock().getLocation().add( 0, 1, 0 ), new Object[]{ event.getPlayer().getName(), BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getTeam() } );
        Sheep spawn = event.getPlayer().getWorld().spawn(  event.getClickedBlock().getLocation().add( 0, 1, 0 ), Sheep.class );
        if ( spawn == null ) event.getPlayer().sendMessage( "Sheep is null LUL" );
    }


}
