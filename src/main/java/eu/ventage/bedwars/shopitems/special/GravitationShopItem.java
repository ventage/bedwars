package eu.ventage.bedwars.shopitems.special;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.items.ItemBuilder;
import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class GravitationShopItem extends ShopItem implements Listener {

    public GravitationShopItem( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @Override
    public String displayName( ) {
        return "";
    }

    @Override
    public Material material( ) {
        return Material.FIREWORK_CHARGE;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.BRONZE;
    }

    @Override
    public int price( ) {
        return 1;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.SPECIAL;
    }

    @EventHandler
    public void onUse( PlayerInteractEvent event ) {
        if ( event.getItem() == null ) return;
        if ( !event.getItem().getType().equals( material() ) ) return;
        ItemStack itemStack = event.getItem();
        itemStack.setAmount( 1 );
        int amount = event.getPlayer().getItemInHand().getAmount();
        event.getPlayer().getItemInHand().setAmount( amount - 1 );
        Item item = event.getPlayer().getWorld().dropItem( event.getPlayer().getLocation(), itemStack );
        item.setPickupDelay( Integer.MAX_VALUE );
        item.setVelocity( event.getPlayer().getLocation().getDirection().normalize().multiply( 2 ) );
        new BukkitRunnable() {
            double phi = 0;

            @Override
            public void run( ) {
                phi += Math.PI / 10;
                for ( double theta = 0; theta <= 2 * Math.PI; theta += Math.PI / 40 ) {
                    double radius = 2.5;
                    double x = radius * Math.cos( theta ) * Math.sin( phi );
                    double y = radius * Math.cos( phi ) + 1.5;
                    double z = radius * Math.sin( theta ) * Math.sin( phi );
                    Location location = ( item.getLocation() ).add( x, y, z );
                    Block block = location.getBlock();
                    block.setType( Material.AIR );
                }
                if ( phi > Math.PI ) {
                    item.remove();
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously( BedWars.getInstance(), 0, 1 );
    }

}
