package eu.ventage.bedwars.shopitems.special;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.maps.Map;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.shopitems.ShopItem;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.minecraft.server.v1_8_R3.PacketPlayOutWorldParticles;
import org.bukkit.*;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TeleporterShopItem extends ShopItem implements Listener {

    public TeleporterShopItem( ) {
        Bukkit.getPluginManager().registerEvents( this, BedWars.getInstance() );
    }

    @Override
    public String displayName( ) {
        return "§aWarp-Powder";
    }

    @Override
    public Material material( ) {
        return Material.SULPHUR;
    }

    @Override
    public int amount( ) {
        return 1;
    }

    @Override
    public int subID( ) {
        return 0;
    }

    @Override
    public PriceItem priceItem( ) {
        return PriceItem.IRON;
    }

    @Override
    public int price( ) {
        return 3;
    }

    @Override
    public String[] lore( ) {
        return new String[0];
    }

    @Override
    public Category category( ) {
        return Category.SPECIAL;
    }

    private HashMap<Player, BukkitTask> hashMap = new HashMap<>();
    private HashMap<Player, Integer> count = new HashMap<>();
    private List<Player> animation = new ArrayList<>();

    @EventHandler
    public void onInteract( PlayerInteractEvent event ) {
        if ( event.getAction().equals( Action.RIGHT_CLICK_AIR )
                || event.getAction().equals( Action.RIGHT_CLICK_BLOCK ) ) {
            if ( event.getMaterial().equals( material() ) ) {
                if ( animation.contains( event.getPlayer() ) || count.get( event.getPlayer() ) != null && count.get( event.getPlayer() ) > 0 ) {
                    event.getPlayer().sendMessage( Data.prefix + "§7Du hast das Item deaktiviert" );
                    count.remove( event.getPlayer() );
                    hashMap.get( event.getPlayer() ).cancel();
                    hashMap.remove( event.getPlayer() );
                    animation.remove( event.getPlayer() );
                    return;
                }
                count.put( event.getPlayer(), 5 );
                sphere( event.getPlayer(), event.getPlayer().getLocation() );
                BukkitTask bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously( BedWars.getInstance(), new Runnable() {
                    @Override
                    public void run( ) {
                        if ( !count.containsKey( event.getPlayer() ) ) return;
                        int integer = count.get( event.getPlayer() );
                        integer--;
                        System.out.println( integer );
                        count.put( event.getPlayer(), integer );
                        if ( integer == 0 ) {
                            Map map = BedWars.getInstance().getMapManager().getActiveMap();
                            event.getPlayer().teleport( map.getTeams().get( BedWarsPlayer.getBedWarsPlayer( event.getPlayer().getUniqueId() ).getTeam() - 1 ) );
                            event.getPlayer().playSound( event.getPlayer().getLocation(), Sound.LEVEL_UP, 1, 1 );
                            count.remove( event.getPlayer() );
                            hashMap.get( event.getPlayer() ).cancel();
                            hashMap.remove( event.getPlayer() );
                            animation.remove( event.getPlayer() );
                            return;
                        }
                    }
                }, 1, 20 );
                hashMap.put( event.getPlayer(), bukkitTask );
            }
        }
    }

    @EventHandler
    public void onMove( PlayerMoveEvent event ) {
        if ( hashMap.containsKey( event.getPlayer() ) ) {
            hashMap.get( event.getPlayer() ).cancel();
            hashMap.remove( event.getPlayer() );
            animation.remove( event.getPlayer() );
            event.getPlayer().sendMessage( Data.prefix + "§7Du hast dich bewegt. Dein Teleport wurde §c§oabgebrochen§7." );
        }
    }

    private void createParticle( EnumParticle particle, Location location, double x, double y, double z ) {
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles( particle, true, (float) ( (float) location.getX() + x ), (float) ( (float) location.getY() + y ), (float) ( (float) location.getZ() + z ), 0, 0, 0, 0, 1 );
        for ( Player player : Bukkit.getOnlinePlayers() ) {
            ( (CraftPlayer) player ).getHandle().playerConnection.sendPacket( packet );
        }
    }

    private void sphere( Player player, Location location ) {
        if ( animation.contains( player ) ) return;
        animation.add( player );
        new BukkitRunnable() {
            double phi = 0;

            @Override
            public void run( ) {
                if ( !hashMap.containsKey( player ) ) this.cancel();
                phi += Math.PI / 10;
                for ( double theta = 0; theta <= 2 * Math.PI; theta += Math.PI / 80 ) {
                    double radius = 1.5;
                    double x = radius * Math.cos( theta ) * Math.sin( phi );
                    double y = radius * Math.cos( phi ) + 1.5;
                    double z = radius * Math.sin( theta ) * Math.sin( phi );
                    createParticle( EnumParticle.DRIP_LAVA, location, x, y, z );
                }
                if ( phi > Math.PI ) {
                    this.cancel();
                    animation.remove( player );
                }
            }
        }.runTaskTimerAsynchronously( BedWars.getInstance(), 0, 1 );
    }

}
