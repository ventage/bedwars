package eu.ventage.bedwars.shopitems;

import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.Material;

public interface ChangableShopItem {

    public Material material( BedWarsPlayer bedWarsPlayer );

}
