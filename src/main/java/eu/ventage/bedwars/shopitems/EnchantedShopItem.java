package eu.ventage.bedwars.shopitems;

import org.bukkit.enchantments.Enchantment;

import java.util.HashMap;

public interface EnchantedShopItem {

    HashMap<Enchantment, Integer> enchantmentList( );

}
