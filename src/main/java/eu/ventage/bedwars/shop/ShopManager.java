package eu.ventage.bedwars.shop;

import eu.ventage.bedwars.shopitems.Category;
import eu.ventage.bedwars.shopitems.ShopItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShopManager {

    HashMap<Category, List<ShopItem>> items;

    public ShopManager( ) {
        items = new HashMap<>();
        for ( Category category : Category.values() ) {
            items.put( category, new ArrayList<>() );
        }
    }

    public void registerShopItem( ShopItem shopItem ) {
        items.get( shopItem.category() ).add( shopItem );
    }


    public List<ShopItem> getItemsFromCategory( Category category ) {
        return items.get( category );
    }
}
