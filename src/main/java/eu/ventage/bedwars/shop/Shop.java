package eu.ventage.bedwars.shop;

import lombok.Getter;

public enum Shop {

    OLD( "Alter Shop" ),
    NEW( "Neuer Shop" );

    @Getter
    private String name;

    Shop( String name ) {
        this.name = name;
    }

}
