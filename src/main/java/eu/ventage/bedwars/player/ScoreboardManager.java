package eu.ventage.bedwars.player;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.scoreboard.ScoreboardUser;
import eu.ventage.bedwars.scoreboard.SidebarBuilder;
import eu.ventage.bedwars.scoreboard.SidebarManager;
import eu.ventage.bedwars.scoreboard.SmartScoreboard;
import eu.ventage.bedwars.teams.Team;
import eu.ventage.bedwars.teams.TeamManager;
import eu.ventage.bedwars.utils.Rank;

import java.util.*;

public class ScoreboardManager {

    private ScoreboardUser scoreboardUser;
    private BedWarsPlayer bedWarsPlayer;

    public ScoreboardManager( BedWarsPlayer bedWarsPlayer ) {
        this.scoreboardUser = SmartScoreboard.getUser( bedWarsPlayer.getPlayer() );
        this.bedWarsPlayer = bedWarsPlayer;
        this.update();
        this.updateSideboard();
    }

    public void update( ) {
        switch ( BedWars.getInstance().getGameState() ) {
            case LOBBY:
                if ( !TeamManager.hasTeam( bedWarsPlayer.getPlayer() ) ) {
                    Rank rank = Rank.getTeamByPlayerPermission( bedWarsPlayer.getPlayer() );
                    scoreboardUser.setPrefix( rank.getPrefix() + " §8| §7" ).setOrder( rank.getOrder() );
                    return;
                }
                Team team = Team.getTeamByNumber( bedWarsPlayer.getTeam() );
                scoreboardUser.setOrder( team.getTeam() ).setPrefix( team.getColor() + "⬛ §8● " + team.getColor() );
                break;
            case INGAME:
                team = Team.getTeamByNumber( bedWarsPlayer.getTeam() );
                if ( team == null ) {
                    scoreboardUser.setOrder( 99 ).setPrefix( "§7Spec §8● §7" );
                    break;
                }
                scoreboardUser.setOrder( team.getTeam() ).setPrefix( team.getColor() + "⬛ §8● " + team.getColor() );
                break;
            case END:
                Rank rank = Rank.getTeamByPlayerPermission( bedWarsPlayer.getPlayer() );
                scoreboardUser.setPrefix( rank.getPrefix() + " §8| §7" ).setOrder( rank.getOrder() );
                break;
        }
    }

    public void updateSideboard( ) {
        SidebarBuilder sidebarBuilder = new SidebarBuilder().setTitle( "§8» §bVentage §8«" );
        switch ( BedWars.getInstance().getGameState() ) {
            case LOBBY:
                sidebarBuilder.add( "§f", "§fMap:", "§b" + BedWars.getInstance().getMapManager().getActiveMap().getMapName(), "§f§f" );
                break;
            case INGAME:
                sidebarBuilder.add( "§f" );
                TeamManager teamManager = BedWars.getInstance().getTeamManager();
                Collection<Double> teams = new ArrayList<>();
                System.out.println( teamManager.getTeams().size() );
                for ( Team team : teamManager.getTeams() )
                    teams.add( Double.valueOf( teamManager.getPlayers( team ).size() + "." + team.getTeam() ) );
                System.out.println( teams.size() );
                for ( int i = 0; i < teamManager.getTeams().size(); i++ ) {
                    Double max = Collections.max( teams );
                    String[] values = max.toString().split( "\\." );
                    int players = Integer.parseInt( values[0] );
                    teams.remove( max );
                    Team team = Team.getTeamByNumber( Integer.valueOf( values[1] ) );
                    sidebarBuilder.add( ( teamManager.hasBed( team ) ? "§a❤" : "§c✖" ) + " " + team.getColor() + team.getName() + " §7- " + players );
                }
                sidebarBuilder.add( "§7§m-------------" );
                sidebarBuilder.add( "§7Kills:" );
                sidebarBuilder.add( "§e" + bedWarsPlayer.getStats().getTempKills() );
                sidebarBuilder.add( "§f§f" );
                sidebarBuilder.add( "§7Abgebaute Betten:" );
                sidebarBuilder.add( "§c" + bedWarsPlayer.getStats().getTempBedsDestroyed() );
                break;
            case END:
                sidebarBuilder.add( "§f" );
                sidebarBuilder.add( "§fGewinner:" );
                teamManager = BedWars.getInstance().getTeamManager();
                Team team = null;
                for ( Team team1 : teamManager.getTeams() )
                    if ( teamManager.getPlayers( team1 ).size() > 0 ) team = team1;
                sidebarBuilder.add( team.getColor() + "Team " + team.getName() );
                sidebarBuilder.add( "§f§f", "§bVentage.eu", "§f§f§f" );
                break;
        }
        sidebarBuilder.send( bedWarsPlayer.getPlayer() );
    }

    public void remove( ) {
        SmartScoreboard.removeUser( bedWarsPlayer.getPlayer() );
    }
}
