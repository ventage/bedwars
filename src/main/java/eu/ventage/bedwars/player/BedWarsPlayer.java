package eu.ventage.bedwars.player;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import eu.ventage.bedwars.inventory.ShopInventory;
import eu.ventage.bedwars.items.StickItem;
import eu.ventage.bedwars.runnable.LobbyCountdown;
import eu.ventage.bedwars.shop.Shop;
import eu.ventage.bedwars.stats.Stats;
import eu.ventage.bedwars.utils.Actionbar;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;
import java.util.concurrent.ExecutionException;

@Getter
@Setter
public class BedWarsPlayer {

    private final static LoadingCache<UUID, BedWarsPlayer> playerCache = CacheBuilder.newBuilder().build( new CacheLoader<UUID, BedWarsPlayer>() {
        @Override
        public BedWarsPlayer load( UUID uuid ) throws Exception {
            return new BedWarsPlayer( uuid );
        }
    } );

    public static BedWarsPlayer getBedWarsPlayer( UUID uuid ) {
        try {
            return playerCache.get( uuid );
        } catch ( ExecutionException e ) {
            e.printStackTrace();
        }
        return new BedWarsPlayer( uuid );
    }

    /*------------------------------------------------------------------------------------------------------------------------------------------------*/

    private final UUID uuid;
    private final Player player;
    private Stats stats;
    private ScoreboardManager scoreboardManager;
    private Actionbar actionbar;
    private ShopInventory shopInventory;
    private boolean buildMode;
    private Shop shop;
    private StickItem stickItem;
    private int team, kills, bedsDestroyed;
    private Player lastDamager;
    private long lastDamagerTime;

    private BedWarsPlayer( UUID uuid ) {
        this.uuid = uuid;
        this.player = Bukkit.getPlayer( uuid );
        this.stickItem = StickItem.STICK;
        this.shop = Shop.OLD;
        this.buildMode = false;
    }

    public void set( ) {
        this.team = -1;
        this.stats = Stats.getStats( uuid );
        this.scoreboardManager = new ScoreboardManager( this );
        this.actionbar = new Actionbar( player, "§7Kein Team", 1 );
        this.shopInventory = new ShopInventory( this );
    }

    public void unregister( ) {
        playerCache.invalidate( uuid );
    }


}
