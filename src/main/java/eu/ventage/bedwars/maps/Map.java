package eu.ventage.bedwars.maps;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.teams.TeamSizes;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.configuration.file.FileConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Map {

    @Getter
    private String mapName;
    @Getter
    private TeamSizes teamSizes;
    @Getter
    private Location spectator;
    @Getter
    private List<Location> teams, bronzeSpawner, ironSpawner, goldSpawner, villager;
    @Getter
    private FileConfiguration config;
    @Getter
    private World world;

    public Map( String mapName, TeamSizes teamSizes, FileConfiguration config ) {
        Bukkit.createWorld( new WorldCreator( mapName ) );
        this.world = Bukkit.getWorld( mapName );
        this.mapName = mapName;
        this.teamSizes = teamSizes;
        this.config = config;
        this.teams = new ArrayList<>();
        this.bronzeSpawner = new ArrayList<>();
        this.ironSpawner = new ArrayList<>();
        this.goldSpawner = new ArrayList<>();
        this.villager = new ArrayList<>();
        loadSpawns();
    }

    private void loadSpawns( ) {
        if ( config.get( "Spawn.Spectator." + mapName ) != null )
            spectator = (Location) config.get( "Spawn.Spectator." + mapName );
        if ( config.get( "Spawn.Teams." + mapName ) != null )
            teams = (List<Location>) config.get( "Spawn.Teams." + mapName );
        if ( config.get( "Spawn.BronzeSpawner." + mapName ) != null )
            bronzeSpawner = (List<Location>) config.get( "Spawn.BronzeSpawner." + mapName );
        if ( config.get( "Spawn.IronSpawner." + mapName ) != null )
            ironSpawner = (List<Location>) config.get( "Spawn.IronSpawner." + mapName );
        if ( config.get( "Spawn.GoldSpawner." + mapName ) != null )
            goldSpawner = (List<Location>) config.get( "Spawn.GoldSpawner." + mapName );
        if ( config.get( "Spawn.Villager." + mapName ) != null )
            villager = (List<Location>) config.get( "Spawn.Villager." + mapName );
    }

    public void addTeamLocation( Location location ) {
        teams.add( location );
    }

    public void setSpectatorSpawn( Location location ) {
        spectator = location;
    }

    public void addBronzeLocation( Location location ) {
        bronzeSpawner.add( location );
    }

    public void addIronLocation( Location location ) {
        ironSpawner.add( location );
    }

    public void addGoldLocation( Location location ) {
        goldSpawner.add( location );
    }

    public void addVillagerLocation( Location location ) {
        villager.add( location );
    }

    public void save( ) {
        config.set( "Spawn.Teams." + mapName, teams );
        config.set( "Spawn.BronzeSpawner." + mapName, bronzeSpawner );
        config.set( "Spawn.IronSpawner." + mapName, ironSpawner );
        config.set( "Spawn.GoldSpawner." + mapName, goldSpawner );
        config.set( "Spawn.Spectator." + mapName, spectator );
        config.set( "Spawn.Villager." + mapName, villager );
        config.set( "Spawn.Lobby", BedWars.getInstance().getMapManager().getLobby() );
        try {
            config.save( new File( BedWars.getInstance().getDataFolder(), "maps.yml" ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

}
