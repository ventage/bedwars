package eu.ventage.bedwars.maps;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.items.ItemBuilder;
import eu.ventage.bedwars.shopitems.PriceItem;
import eu.ventage.bedwars.teams.TeamSizes;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitTask;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class MapManager {

    @Getter
    private File file;
    @Getter
    private FileConfiguration config;
    @Getter
    private List<Map> maps;
    @Getter
    private Map activeMap;
    @Getter
    private BukkitTask spawner;
    @Getter
    @Setter
    private Location lobby;

    public MapManager( ) {
        file = new File( BedWars.getInstance().getDataFolder(), "maps.yml" );
        config = new YamlConfiguration();
        boolean newFile;
        try {
            newFile = file.createNewFile();
            config.load( file );
        } catch ( IOException | InvalidConfigurationException e ) {
            e.printStackTrace();
            newFile = false;
        }
        if ( newFile ) {
            Location location = new Location( Bukkit.getWorlds().get( 0 ), 0, 0, 0 );
            config.addDefault( "Maps", Arrays.asList( Bukkit.getWorlds().get( 0 ).getName() ) );
            config.addDefault( "MapSettings.world", TeamSizes.T4x2.name() );
            config.addDefault( "Spawn.world", Arrays.asList( location, location ) );
            config.addDefault( "Spawn.Teams.world", Arrays.asList( location, location ) );
            config.addDefault( "Spawn.IronSpawner.world", Arrays.asList( location, location ) );
            config.addDefault( "Spawn.BronzeSpawner.world", Arrays.asList( location, location ) );
            config.addDefault( "Spawn.GoldSpawner.world", Arrays.asList( location, location ) );
            config.addDefault( "Spawn.Villager.world", Arrays.asList( location, location ) );
            config.options().copyDefaults( true );
            saveFile();
        }
        maps = new ArrayList<>();
        loadMaps();
    }

    private void loadMaps( ) {
        lobby = (Location) config.get( "Spawn.Lobby" );
        List<String> mapList = (List<String>) config.get( "Maps" );
        for ( String s : mapList )
            maps.add( new Map( s, TeamSizes.getTeamSizeByString( config.getString( "MapSettings." + s ) ), config ) );
        if ( maps.size() < 1 ) Bukkit.getPluginManager().disablePlugin( BedWars.getInstance() );
        activeMap = maps.get( new Random().nextInt( maps.size() ) );
    }

    public void startSpawning( ) {
        spawner = Bukkit.getScheduler().runTaskTimerAsynchronously( BedWars.getInstance(), new Runnable() {
            int i = 0;

            @Override
            public void run( ) {
                i++;
                if ( !activeMap.getBronzeSpawner().isEmpty() ) {
                    for ( Location location : activeMap.getBronzeSpawner() ) {
                        Bukkit.getScheduler().runTask( BedWars.getInstance(), ( ) -> {
                            if ( location != null )
                                Bukkit.getWorld( getActiveMap().getMapName() ).dropItem( location, new ItemBuilder( PriceItem.BRONZE.getMaterial() ).setDisplayName( PriceItem.BRONZE.getName() ).build() );
                        } );
                    }
                }
                if ( i == 10 || i == 20 || i == 30 ) {
                    if ( !activeMap.getIronSpawner().isEmpty() ) {
                        for ( Location location : activeMap.getIronSpawner() ) {
                            Bukkit.getScheduler().runTask( BedWars.getInstance(), ( ) -> {
                                if ( location != null )
                                    Bukkit.getWorld( getActiveMap().getMapName() ).dropItem( location, new ItemBuilder( PriceItem.IRON.getMaterial() ).setDisplayName( PriceItem.IRON.getName() ).build() );
                            } );
                        }
                    }
                }
                if ( i == 30 ) {
                    i = 0;
                    if ( !activeMap.getGoldSpawner().isEmpty() ) {
                        for ( Location location : activeMap.getGoldSpawner() ) {
                            Bukkit.getScheduler().runTask( BedWars.getInstance(), ( ) -> {
                                if ( location != null )
                                    Bukkit.getWorld( getActiveMap().getMapName() ).dropItem( location, new ItemBuilder( PriceItem.GOLD.getMaterial() ).setDisplayName( PriceItem.GOLD.getName() ).build() );
                            } );
                        }
                    }
                }
            }
        }, 1, 20 );
    }

    public void saveFile( ) {
        try {
            config.save( file );
        } catch ( IOException | NullPointerException e ) {
            e.printStackTrace();
            Bukkit.getConsoleSender().sendMessage( "§4Config konnte nicht gespeichert werden. ERROR HELP AHHHH ALLES EXPLODIERT" );
        }
    }
}
