package eu.ventage.bedwars.entity;

import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.*;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.HashMap;

public class EntityBuilder {

    private EntityType entityType;
    private ArrayList<PotionEffect> potionEffects;
    private String displayName;
    private boolean noAI;
    private double health;
    private HashMap<String, String> nbtTags;

    public EntityBuilder( EntityType entityType ) {
        this.entityType = entityType;
        this.potionEffects = new ArrayList<>();
        this.nbtTags = new HashMap<>();
        this.noAI = false;
        this.health = -1;
        this.setDisplayName( "" );
    }

    public Entity spawn( Location location ) {
        Entity entity = location.getWorld().spawn( location, entityType.getEntityClass() );
        entity.setCustomName( displayName );
        entity.setCustomNameVisible( true );
        if ( entity instanceof LivingEntity ) {
            potionEffects.forEach( ( (LivingEntity) entity )::addPotionEffect );
            if ( health > 0 ) ( (LivingEntity) entity ).setMaxHealth( health );
        }
        net.minecraft.server.v1_8_R3.Entity nmsEntity = ( (CraftEntity) entity ).getHandle();
        NBTTagCompound nbtTag = nmsEntity.getNBTTag();
        if ( nbtTag == null ) nbtTag = new NBTTagCompound();
        setNBTTags( nmsEntity, nbtTag );
        return entity;
    }

    private void setNBTTags( net.minecraft.server.v1_8_R3.Entity nmsEntity, NBTTagCompound nbtTag ) {
        nmsEntity.c( nbtTag );
        nbtTag.setInt( "NoAI", ( noAI ? 1 : 0 ) );
        nbtTags.forEach( ( String s, String s2 ) -> {
            if ( s2.equalsIgnoreCase( "true" ) || s2.equalsIgnoreCase( "false" ) )
                nbtTag.setBoolean( s, s2.equalsIgnoreCase( "true" ) );
            else if ( isInt( s2 ) )
                nbtTag.setInt( s, Integer.parseInt( s2 ) );
            else
                nbtTag.setString( s, s2 );
        } );
        nmsEntity.f( nbtTag );
    }

    private boolean isInt( String string ) {
        try {
            Integer.parseInt( string );
            return true;
        } catch ( NumberFormatException e ) {
            return false;
        }
    }

    public EntityBuilder setDisplayName( String displayName ) {
        this.displayName = displayName;
        return this;
    }

    public EntityBuilder addPotionEffect( PotionEffect effect ) {
        potionEffects.add( effect );
        return this;
    }

    public EntityBuilder setNoAI( boolean boo ) {
        noAI = boo;
        return this;
    }

    public EntityBuilder setNBTTag( String key, String value ) {
        nbtTags.put( key, value );
        return this;
    }

    public EntityBuilder setHealth( double health ) {
        this.health = health;
        return this;
    }


}
