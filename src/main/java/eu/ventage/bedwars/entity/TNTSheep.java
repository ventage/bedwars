package eu.ventage.bedwars.entity;

import eu.ventage.bedwars.BedWars;
import lombok.Getter;
import net.minecraft.server.v1_8_R3.EntityInsentient;
import net.minecraft.server.v1_8_R3.PathEntity;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftSheep;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.scheduler.BukkitTask;

public class TNTSheep {

    @Getter
    private String owner;
    @Getter
    private Sheep sheep;
    @Getter
    private TNTPrimed tnt;
    private BukkitTask bukkitTask;
    private Location lastLocation, targetLocation;

    public TNTSheep( Sheep sheep, String owner ) {
        this.owner = owner;
        this.sheep = sheep;
        this.tnt = sheep.getLocation().getWorld().spawn( sheep.getLocation(), TNTPrimed.class );
        this.tnt.setFuseTicks( 20 * 1000 );
        this.tnt.setCustomName( owner );
        this.targetLocation = null;
        this.lastLocation = sheep.getLocation();
        this.activateTimer();
    }

    public void move( ) {
        if ( tnt == null ) return;
        sheep.setHealth( 0 );
        tnt.teleport( sheep );
    }

    public void explode( ) {
        bukkitTask.cancel();
        this.sheep.setHealth( 0 );
    }

    public void setPath( Location location ) {
        targetLocation = location;
        PathEntity pathEntity = ( (EntityInsentient) sheep ).getNavigation().a( location.getX(), location.getY(), location.getZ() );
        ( (EntityInsentient) sheep ).getNavigation().a( pathEntity, 1.0D );
        ( (EntityInsentient) sheep ).getNavigation().a( 2.0D );
    }

    public void activateTimer( ) {
        bukkitTask = Bukkit.getScheduler().runTaskTimerAsynchronously( BedWars.getInstance(), ( ) -> {
            if ( !lastLocation.equals( sheep.getLocation() ) )
                if ( sheep.getHealth() > 0 && !tnt.isDead() ) move();
        }, 20, 1 );
    }

}
