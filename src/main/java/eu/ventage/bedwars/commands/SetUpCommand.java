package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.maps.Map;
import eu.ventage.bedwars.maps.MapManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

public class SetUpCommand implements CommandExecutor {
    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {

        if ( sender instanceof Player ) {
            MapManager mapManager = BedWars.getInstance().getMapManager();
            Map map = mapManager.getActiveMap();
            switch ( args.length ) {
                case 1:
                    Location location = ( (Player) sender ).getLocation();
                    if ( args[0].equalsIgnoreCase( "setLobby" ) ) {
                        mapManager.setLobby( location );
                        sender.sendMessage( Data.prefix + "§7Du hast die Lobby gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "addTeam" ) ) {
                        map.addTeamLocation( location );
                        sender.sendMessage( Data.prefix + "§7Du hast das Team #§e" + map.getTeams().size() + " §7gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "setSpectator" ) ) {
                        map.setSpectatorSpawn( location );
                        sender.sendMessage( Data.prefix + "§7Du hast den Spectator Spawn gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "addVillager" ) ) {
                        map.addVillagerLocation( location );
                        sender.sendMessage( Data.prefix + "§7Du hast den Villager #§e" + map.getVillager().size() + " §7gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "addBronze" ) ) {
                        map.addBronzeLocation( location );
                        sender.sendMessage( Data.prefix + "§7Du hast den Bronze Spawner #§e" + map.getBronzeSpawner().size() + " §7gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "addIron" ) ) {
                        map.addIronLocation( location );
                        sender.sendMessage( Data.prefix + "§7Du hast den Eisen Spawner #§e" + map.getIronSpawner().size() + " §7gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "addGold" ) ) {
                        map.addGoldLocation( location );
                        sender.sendMessage( Data.prefix + "§7Du hast den Gold Spawner #§e" + map.getGoldSpawner().size() + " §7gesetzt" );
                        return false;
                    } else if ( args[0].equalsIgnoreCase( "save" ) ) {
                        map.save();
                        sender.sendMessage( Data.prefix + "§7wurde gespeichert" );
                        return false;
                    }
                    break;
                default:
                    sendHelp( sender );
                    break;
            }
        }
        return false;
    }

    public void sendHelp( CommandSender sender ) {
        sender.sendMessage( "§c§nSetup:" );
        sender.sendMessage( "/setup setLobby" );
        sender.sendMessage( "/setup addTeam" );
        sender.sendMessage( "/setup addBed1" );
        sender.sendMessage( "/setup addBed2" );
        sender.sendMessage( "/setup setSpectator" );
        sender.sendMessage( "/setup addVillager" );
        sender.sendMessage( "/setup addBronze" );
        sender.sendMessage( "/setup addIron" );
        sender.sendMessage( "/setup addGold" );
        sender.sendMessage( "/setup save" );
        sender.sendMessage( "§c§nSetup:" );
    }

}
