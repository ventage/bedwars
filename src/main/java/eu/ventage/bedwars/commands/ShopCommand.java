package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.shop.Shop;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShopCommand implements CommandExecutor {
    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {
        if ( sender instanceof Player ) {
            BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( ( (Player) sender ).getUniqueId() );
            Shop newShop = bedWarsPlayer.getShop().equals( Shop.NEW ) ? Shop.OLD : Shop.NEW;
            bedWarsPlayer.setShop( newShop );
            sender.sendMessage( Data.prefix + "§7Du hast den " + ( newShop.equals( Shop.NEW ) ? "§aneuen" : "§aalten" ) + " §7Shop ausgewählt" );
        }
        return false;
    }
}
