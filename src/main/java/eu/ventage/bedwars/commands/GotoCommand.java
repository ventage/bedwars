package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GotoCommand implements CommandExecutor {

    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {

        if ( args.length == 0 ) {

            if ( sender instanceof Player ) {
                Player player = (Player) sender;
                player.teleport( Bukkit.getWorld( BedWars.getInstance().getMapManager().getActiveMap().getMapName() ).getSpawnLocation() );
                player.sendMessage( Data.prefix + "§7Du bist nun auf der §bActiveMap" );
            }
            return true;
        }

        return false;
    }

}
