package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.player.BedWarsPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BuildCommand implements CommandExecutor {

    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {

        if ( sender instanceof Player ) {

            Player player = (Player) sender;
            BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( player.getUniqueId() );
            bedWarsPlayer.setBuildMode( !bedWarsPlayer.isBuildMode() );
            player.sendMessage( Data.prefix + "§7Du hast den BuildMode " + ( bedWarsPlayer.isBuildMode() ? "§aAktiviert" : "§cDeaktiviert" ) );

        }

        return false;
    }
}
