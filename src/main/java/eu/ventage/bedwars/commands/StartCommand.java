package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.BedWars;
import eu.ventage.bedwars.Data;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StartCommand implements CommandExecutor {
    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {

        BedWars.getInstance().getLobbyCountdown().setNow( 11 );
        sender.sendMessage( Data.prefix + "§7Du hast das Spiel §a§ogestartet" );

        return false;
    }
}
