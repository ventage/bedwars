package eu.ventage.bedwars.commands;

import eu.ventage.bedwars.Data;
import eu.ventage.bedwars.fetcher.UUIDFetcher;
import eu.ventage.bedwars.player.BedWarsPlayer;
import eu.ventage.bedwars.stats.Stats;
import eu.ventage.bedwars.utils.Rank;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.UUID;

public class StatsCommand implements CommandExecutor {

    @Override
    public boolean onCommand( CommandSender sender, Command command, String s, String[] args ) {
        if ( args.length == 1 ) {
            UUID uuid = UUIDFetcher.getUUID( args[0] );
            if ( uuid == null ) {
                sender.sendMessage( Data.prefix + "§7Der Spieler existiert nicht" );
                return false;
            }
            Stats stats = Stats.getStats( UUIDFetcher.getUUID( args[0] ) );
            if ( stats == null ) {
                sender.sendMessage( Data.prefix + "§7Der Spieler hat keine Stats" );
                return false;
            }
            sendStats( sender, args[0], stats );
            return false;
        } else if ( args.length == 0 ) {
            if ( sender instanceof Player ) {
                BedWarsPlayer bedWarsPlayer = BedWarsPlayer.getBedWarsPlayer( ( (Player) sender ).getUniqueId() );
                Stats stats = bedWarsPlayer.getStats();
                sendStats( sender, bedWarsPlayer.getPlayer().getName(), stats );
            }
            return false;
        }
        sendHelp( sender );
        return false;
    }

    private void sendHelp( CommandSender sender ) {
        sender.sendMessage( Data.prefix + "§7/stats <§aname§7>" );
    }

    private void sendStats( CommandSender sender, String name, Stats stats ) {
        sender.sendMessage( "" );
        sender.sendMessage( "§7Stats von " + Rank.getTeamByPlayerPermission( Bukkit.getPlayer( name ) ).getColor() + name );
        try {
            sender.sendMessage( "§7Position im Ranking: §e" + stats.getRank() );
        } catch ( SQLException e ) {
            sender.sendMessage( "Position im Ranking: §4ERROR" );
        }
        sender.sendMessage( "§7Kills: §e" + stats.getKills() );
        sender.sendMessage( "§7Deaths: §e" + stats.getDeaths() );
        sender.sendMessage( "§7K/D: §e" + stats.getKD() );
        sender.sendMessage( "§7Spiele gespielt: §e" + stats.getPlayedGames() );
        sender.sendMessage( "§7Spiele gewonnen: §e" + stats.getGamesWon() );
        sender.sendMessage( "§7Betten Zerstört: §e" + stats.getDestroyedBeds() );
        sender.sendMessage( "§7Gewinn Chance: §e" + stats.getWinPercentage() + "%" );
        sender.sendMessage( "" );
    }


}
